//This is for export functionality only
if(tblexport == true){
	$(document).ready(function(){
		 console.log('js/common/listing.js');
		  $('#myTable').DataTable();
		  $(document).ready(function() {
			var table = $('#example').DataTable({
			  "columnDefs": [
			  { "visible": false, "targets": 2 }
			  ],
			  "order": [[ 2, 'asc' ]],
			  "displayLength": 25,
			  "drawCallback": function ( settings ) {
				var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last=null;

				api.column(2, {page:'current'} ).data().each( function ( group, i ) {
				  if ( last !== group ) {
					$(rows).eq( i ).before(
					  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
					  );

					last = group;
				  }
				} );
			  }
			} );

		// Order by the grouping
		$('#example tbody').on( 'click', 'tr.group', function () {
		  var currentOrder = table.order()[0];
		  if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
			table.order( [ 2, 'desc' ] ).draw();
		  }
		  else {
			table.order( [ 2, 'asc' ] ).draw();
		  }
		});
	  });
	});
	$('#example23').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});

	//Delete row
	function deleteRow(id,row){		
		if(confirm('Are you sure to delete..!') == true){
			var val = $('#delete'+row).val();
			var fctn = $('#delete'+row).attr('data-url');
			var data = {
						'id': id,
						'row': row
						};
			// console.log(data);
				$.ajax({
						type: "GET",
						url: base_url +''+fctn,
						data: data,
						success:function(data)
						{							
							console.log(data);
							var res = jQuery.parseJSON(data);
							flashmessage(res.status,res.message);
							if(res.status == 'success'){
								$('#delete'+row).parents("tr").remove(); // remove row after delete								
							}
						}
				});
		}
	}
}