// (function poll() {
// 	setTimeout(function() {
// 		$.ajax({ url: base_url +'admin/longPolling', success: function(data) {
// 			 //sales.setValue(data.value);
// 			 console.log(data);
// 		}, dataType: "json", complete: poll });
// 	 }, 5000);
//  })();

function changeSaFlag(id,row){
	if(confirm('Are you sure to change the status..!') == true){
		var val = $('#saflag'+row).val();
		var data = {
					'id': id,
					'status' : val,
					'col' : 'saflag'
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url +'Superadmin/change_saflag',
					data: data,
					success:function(result)
					{
						$('#saflag'+row).parents("tr").remove(); // remove row after delete
						var obj = jQuery.parseJSON(result);
						console.log(obj);
						flashmessage(obj.status,obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	}
}

function sendToSa(id,row){
	if(confirm('Are you sure to send adv to Super Admin..!') == true){
		var val = '1';
		var data = {
					'id': id,
					'status' : val,
					'col' : 'saflag'
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url +'admin/change_saflag',
					data: data,
					success:function(result)
					{
						$('#sendToSa'+row).html('<i class="fa fa-check fa-lg text-dark" title="Advertsiment sent to SuperAdmin for approval" aria-hidden="true"></i>');
						var obj = jQuery.parseJSON(result);
						console.log(obj);
						flashmessage(obj.status,obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	}
}

function changeStatus(id,row){
	
	if(confirm('Are you sure to change the status..!') == true){
		var val = $('#status'+row).val();
		var data = {
					'id': id,
					'status' : val,
					'col' : 'status'
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url +'Adv/change_status',
					data: data,
					success:function(result)
					{
						$('#status'+row).parents("tr").remove(); // remove row after delete
						var obj = jQuery.parseJSON(result);
						console.log(obj);
						flashmessage(obj.status,obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	}
}

// email invoice
function emailInvoice(clientid,row,invoice_no,id){
	if(confirm('Are you sure to send email..!') == true){
		var data = {
					'clientid': clientid,
					'invoice_no': invoice_no,
					'id': id
					};
		$.ajax({
				type: "POST",
				url:  base_url +'Adv/sendInvoice',
				data: data,
				success:function(result)
				{
					var obj = jQuery.parseJSON(result);
					console.log(obj);
					flashmessage(obj.status,obj.message);
				},
				error: function(result) {
					flashmessage('error','No Data to Display, Error Occured..!');
				}
		});
	}
}

function changePayStatus(id,row){
	
	if(confirm('Are you sure to change the Payment Status..!') == true){
		var val = $('#pay_status'+row).val();
		var data = {
					'id': id,
					'status' : val,
					'col' : 'pay_status'
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url +'Adv/change_status',
					data: data,
					success:function(result)
					{
						var obj = jQuery.parseJSON(result);
						console.log(obj);
						flashmessage(obj.status,obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	}
}

function viewContent(id,row,className){
	
	// if(confirm('Are you sure to change the Payment Status..!') == true){
		// var val = $('#pay_status'+row).val();
		var data = {
					'id': id
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url + className +'/viewContent',
					data: data,
					success:function(result)
					{
						var obj = jQuery.parseJSON(result);
						$('#contentTextArea').val(obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	// }
}

function makeClient(id,row,className){
	
	if(confirm('Are you sure to add this Client..!') == true){
		// var val = $('#pay_status'+row).val();
		var data = {
					'id': id
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url + className +'/makeClient',
					data: data,
					success:function(result)
					{
						var obj = jQuery.parseJSON(result);
						// $('#contentTextArea').val(obj.message);
						flashmessage(obj.status,obj.message);
					},
					error: function(result) {
						flashmessage('error','No Data to Display, Error Occured..!');
					}
			});
	}
}

function copyToClipboard(id){	
	$('#'+id).select();
	document.execCommand('copy');
	flashmessage('success','Copied successfully..!');
} 

function updateAdv(id,row){
	
	if(confirm('Are you sure to Update..!') == true){
		var bank_name = $('#bank_name'+row).val();
		var pay_method = $('#pay_method'+row).val();
		var pay_no = $('#pay_no'+row).val();
		// var invoice_no = $('#invoice_no'+row).val();
		var invoice_date = $('#invoice_date'+row).val();
		var supplier_ref = $('#supplier_ref'+row).val();
		var invoice_generate = $('#invoice_generate'+row).val();
		var data = {
					'id': id,
					'bank_name' : bank_name,
					'pay_method' : pay_method,
					'pay_no' : pay_no,
					// 'invoice_no' : invoice_no,
					'invoice_date' : invoice_date,
					'supplier_ref' : supplier_ref,
					'invoice_generate' : invoice_generate
					};
		console.log(data);
			$.ajax({
					type: "POST",
					url:  base_url +'Adv/updateAdv',
					data: data,
					success:function(data)
					{
						alert('Successfully Changed');
						$('#invoice_generate'+row).parents("tr").remove(); // remove row after delete
						
						var obj = jQuery.parseJSON(data);
						if(obj.status == 'fail'){
							var colr = 'danger';
							var symbol = 'ban';
							$('#btn-add-form').attr("disabled",true);
						}else if(obj.status == 'nothing'){
							return false;
						}else{
							var colr = 'success';
							var symbol = 'check';
							$('#btn-add-form').removeAttr('disabled');
							
							var cityArray = jQuery.makeArray(city_id);
							var city_nameArray = jQuery.makeArray(city_name);
							var i;
							$('#demobox').html('');	
							$("#rate").val('');
							//add City wise rate textbox
							for (i = 0; i < cityArray.length; ++i) {						
								$('#demobox').append('<input type="number" class="form-control city_rate" id="city_rate" name="city_rate[]" placeholder="'+city_nameArray[i]+' Rate" value="" required >');
							}
						}
					}
			});
	}
}

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()

function countChar(val) {
	var len = val.value.length;
	var schedule_type = $('#schedule_type').val();
	// alert(schedule_type);
	// alert(len);
	
	switch(schedule_type) {
	  case '':
		$('#content').val('');
		flashmessage('error','Plese select Schedule Type first !');
		break;
	  case 'Text':
		if(len > 2){
			$('#content').val('');
			flashmessage('error','Content should be less than 2000 words!');			
		}
		break;
	  case 'Club':
		// code block
		break;
	  default:
		// code block
	}
	// if (len >= 500) {
	  // val.value = val.value.substring(0, 500);
	// } else {
	  // $('#charNum').text(500 - len);
	// }
};

$(document).ready(function(e) {
	console.log('js/common/adv.js');	
	// $('#date').datepicker({ minDate: 0 });
	$('#time').attr("readonly",true);//readonly time
	$('#city_id').attr("readonly",true);//readonly 
	$('#btn-add-form').attr("disabled",true);
	$('#rate').attr("readonly",true);
	// $('#amt').attr("readonly",true);
	// $('#net_amt').attr("readonly",true);
	
	//remove disabled after date select
	// $('#city_id').change(function() {
		// $('#time option:eq(1)');
	// });
		
	$('#date').change(function() {
		var date = $('#date').val();
		if(date < 1){
			document.getElementById("errordate").textContent = "Please Select Date";
			event.preventDefault();
			return false;
		}else{
			$('#time').attr("readonly",false);
			$('#city_id').removeAttr('readonly');
		}
	});
	
	//return last selected value from select options
	// var $topo = $('#city_id');
	// var valArray = ($topo.val()) ? $topo.val() : [];
	// $topo.change(function() {
		// var val = $(this).val(),
			// numVals = (val) ? val.length : 0,
			// changes;
		// if (numVals != valArray.length) {
			// var longerSet, shortSet;
			// (numVals > valArray.length) ? longerSet = val : longerSet = valArray;
			// (numVals > valArray.length) ? shortSet = valArray : shortSet = val;
			// //create array of values that changed - either added or removed
			// changes = $.grep(longerSet, function(n) {
				// return $.inArray(n, shortSet) == -1;
			// });

			// //var res = logChanges(changes, (numVals > valArray.length) ? 'selected' : 'removed');
			// console.log(changes[0]);

		// }else{
			// //if change event occurs and previous array length same as new value array : items are removed and added at same time
		   // logChanges( valArray, 'removed');
			// logChanges( val, 'selected');
		// }
		// valArray = (val) ? val : [];
	// });
	
	//check booking
	$('#time, #city_id, #date').change(function() {
		var date = $('#date').val();
		var city_id = $('#city_id').val();
		// var city_id = $('#city_id').find('option').filter(':selected:last').val();
		// console.log(city_ids);
		// console.log(city_id);
		$('#demobox').html('');	
		var city_name = [];
		$( "#city_id option:selected" ).each(function() {
		  city_name.push($( this ).text());
		});
		// console.log(city_id);
		// console.log(city_name);
		// var schedule_type = $('#schedule_type').val();
		var time = $('#time').val();
		var data = {
					'date': date,
					'city_id': city_id,
					// 'schedule_type': schedule_type,
					'time': time
					};
		$baselink = base_url;
		$.ajax({
				type: "POST",
				url: $baselink + 'Adv/check_booking',
				data: data,
				success: function(result) {
					// console.log(result);
					var obj = jQuery.parseJSON(result);
					if(obj.status == 'fail'){
						var colr = 'danger';
						var symbol = 'ban';
						$('#btn-add-form').attr("disabled",true);
					}else if(obj.status == 'nothing'){
						return false;
					}else{
						var colr = 'success';
						var symbol = 'check';
						$('#btn-add-form').removeAttr('disabled');
						
						var cityArray = jQuery.makeArray(city_id);
						var city_nameArray = jQuery.makeArray(city_name);
						var i;
						$('#demobox').html('');	
						$("#rate").val('');
						//add City wise rate textbox
						for (i = 0; i < cityArray.length; ++i) {						
							$('#demobox').append('<input type="number" class="form-control city_rate" id="city_rate" name="city_rate[]" placeholder="'+city_nameArray[i]+' Rate" value="" required >');
						}
					}
					$('#resultMsg').html('<div class="alert alert-'+colr+' alert-dismissable"><i class="fa fa-'+symbol+'"></i>'+
										'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
										 obj.message+'</div>');
				}

			});
	});
	
	//count character and display below
	$(document).on("keyup", "#content", function() {
		var len = $('#content').val().length;
		var schedule_type = $('#schedule_type').val();
		if(schedule_type == ''){
			$('#content').val('');
			flashmessage('error','Select Schedule type first..!');			
		}
		$('#ErrorContent').html('Word character : <b>'+len+'</b>');
		
		switch(schedule_type) {
			case 'Full Screen' :
				if(len > 650){
					$('#content').val('');
					flashmessage('error','Input lenght should be less than 650 character');
				}
				break;
			case 'Text' :
				if(len > 2000){
					$('#content').val('');
					flashmessage('error','Input lenght should be less than 2000 character');
				}
				break;
			case 'Club' :
				if(len > 240){
					$('#content').val('');
					flashmessage('error','Input lenght should be less than 240 character');
				}
				break;
			default :
				break;
		}
	});
	
	$(document).on("keyup", ".city_rate", function() {
		var sum = 0;
		$(".city_rate").each(function(){
			sum += +$(this).val();
		});
		$("#rate").val(sum);
		
		//auto cal
		// $('#net_amt').val(sum); //efftect Net Amt
		//Effect 
		// var num = parseFloat(sum);
		// var val = num - (num * .18);
		// var val = ((num * 100) / 118).toFixed(2);
		// $('#amt').val(val);
	});
	
	$('#amt').keyup(function() {
		var amt = $('#amt').val();
		var num = parseFloat(amt);
		var val = num + (num * .18);
		$('#net_amt').val(val);
	});
	
	$('#net_amt').keyup(function() {
		var net_amt = $('#net_amt').val();
		var num = parseFloat(net_amt);
		// var val = num - (num * .18);
		var val = ((num * 100) / 118).toFixed(2);
		$('#amt').val(val);
	});
	
	// $('#apply_gst').change(function() {
		// var net_amt = $('#net_amt').val();
		// var apply = $('#apply_gst').val();
		// var val = '';
		// if( apply == 'yes'){
			// var num = parseFloat(net_amt);
			// val = num - (num * .18);
		// }else{
			// val = net_amt;
		// }
		// $('#amt').val(val);
	// });
	
	// $('#add-form').submit(function() {
		// alert('s');
		// var client_id = $('#client_id').val();
		// var ro_no = $('#ro_no').val();
		// var date = $('#date').val();
		// var rate = $('#rate').val();
		// var net_amt = $('#net_amt').val();
		// var amt = $('#amt').val();
		// var remark = $("#remark").val();
		
		// document.getElementById("errorro_no").textContent = "";
		// document.getElementById("errorclient_id").textContent = "";
		// document.getElementById("errordate").textContent = "";
		// document.getElementById("errorrate").textContent = "";
		// document.getElementById("errornet_amt").textContent = "";
		// document.getElementById("erroramt").textContent = "";
		// document.getElementById("remark").textContent = "";
		
		// var client_idleng = client_id.length;
		// var ro_noleng = ro_no.length;
		// var dateleng = date.length;
		// var rateleng = rate.length;
		// var net_amtleng = net_amt.length;
		// var amtleng = amt.length;
		// var remarkleng = remark.length;
		
		// if(ro_noleng < 1){
			// document.getElementById("errorro_no").textContent = "Enter Ro. No.";
			// event.preventDefault();
			// return false;
		// }else if(client_idleng < 1){
			// document.getElementById("errorclient_id").textContent = "Client is required";
			// event.preventDefault();
			// return false;
		// }else if(dateleng < 1){
			// document.getElementById("errordate").textContent = "Date is required";
			// event.preventDefault();
			// return false;
		// }else if(rateleng < 1){
			// document.getElementById("errorrate").textContent = "Enter Rate";
			// event.preventDefault();
			// return false;
		// }else if(net_amtleng < 1){
			// document.getElementById("errornet_amt").textContent = "Enter Final amount";
			// event.preventDefault();
			// return false;
		// }else if(amtleng < 1){
			// document.getElementById("erroramt").textContent = "Enter Amount";
			// event.preventDefault();
			// return false;
		// }else if(remarkleng < 1){
			// document.getElementById("errorremark").textContent = "Enter Remark";
			// event.preventDefault();
			// return false;
		// }else{
			// return true;
		// }
		// else if(avtarleng < 1){
			// document.getElementById("errorimage").textContent = "Select Image";
			// event.preventDefault();
			// return false;
		// }else(avtarleng > 1){
			// document.getElementById("errorimage").textContent = "";
		// }		
		// if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG"){
	   
			// document.getElementById("errorimage").textContent = "Image not of valid Extension";
			// event.preventDefault();
			// return false;
		// }else{
			 // document.getElementById("errorimage").textContent = "";
		// }
	
	// });
			
});