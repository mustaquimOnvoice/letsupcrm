<title><?php echo $title;?></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title"><?php echo $title;?></h4>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">			
			<div class="row">
			  <div class="col-md-12">
				<div class="text-right">
				  <button id="print" class="btn btn-success btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
				</div>
			  </div>
			</div>
            <div class="row printableArea">
              <div class="col-md-12">
			        	<div class="table-responsive" style="clear: both;">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center"><h5><b>Invoice No: <?php echo '<br>'.$data->invoice_no;?></b></h5></th>
                        <th class="text-center"><h5><b>Invoice Date: <?php echo '<br>'.date('d-M-Y', strtotime($data->date));?></b></h5></th>
                        <th class="text-center"><h5><b>Supplier's Ref: <?php echo '<br>'.$data->supplier_ref;?></b></h5></th>
                        <th class="text-center"><h5><b>Executive Name: <?php echo '<br>'.$data->fname.' '.$data->lname;?></b></h5></th>
                        <th class="text-center"><h5><b>Ro.Code: <?php echo '<br>'.$data->id;?></b></h5></th>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div  class="pull-left">
                  <address>
                  <h3>To,</h3>
                  <h4 class="font-bold"><?php echo $data->business_name;?>,</h4>
                  <p class="font-bold">GST No. <?php echo $data->gst_no;?></p>
                  <p class="text-muted"><?php echo $data->address;?></p>
                  </address>
                </div>
                <div class="pull-right text-right">
                  <address>
				          <p><img src="<?php echo base_url();?>assets/plugins/images/logo/letsup.png" alt="letsup" title="letsup" height="80px" alt="home" /></p>
                  <h4> &nbsp;<b class="text-danger">Hope Technologies</b></h4>
                  <p class="text-muted">144 Namoha Industries Compound, <br/>
                    Next to ST Depot Station Road, <br/>
                    Ahmednagar - 414001, <br/>
                    GSTIN/UIN: 27AAKFH6463J1ZX <br/>
                    Company PAN: AAKFH6463J <br/>
                    State Name: Maharashtra, Code :27
                  </p>
                  </address>
                </div>
              </div>
              <div class="col-md-12">
                <div class="table-responsive" style="clear: both;">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th>Description</th>
                        <th class="text-right">HSN/SAC</th>
                        <th class="text-right">%</th>
                        <th class="text-right">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-center">1</td>
                        <td><?php echo $data->schedule_type;?>
                        <p class="text-muted">
                          Advertisement for <?php $x= 0;
													$cities_id = explode(',',$data->city_id);
													foreach($cities_id as $city_id){
														echo getCityName($cities_id[$x]).', '; 
														$x++;
													}
												?> <br/>
                          Time : <?php echo $data->timming;?><br>
                          Publish Date : <?php echo date('d-M-Y', strtotime($data->date));?>
                        </p>
					            	</td>
                        <td class="text-right">998366 </td>
                        <td class="">  </td>
                        <td class="text-right"> <?php echo $data->amt;?> </td>
                      </tr>
                      <tr>
                        <td class="text-right" colspan="2">OUTPUT CGST </td>
                        <td class="">  </td>
                        <td class="text-right"> 9% </td>
                        <td class="text-right"> <?php echo ($data->net_amt - $data->amt) / 2;?> </td>
                      </tr>
					            <tr>
                        <td class="text-right" colspan="2">OUTPUT SGST </td>
                        <td class="">  </td>
                        <td class="text-right"> 9% </td>
                        <td class="text-right"> <?php echo ($data->net_amt - $data->amt) / 2;?> </td>
                      </tr>
                    </tbody>
					<tfoot>
					  <tr>
                        <th class="text-left" colspan="2">Amount Chargeable (in words): <b id="inwords"></b></th>
                        <th class="text-center"><h5><b>Total :</b></h5></th>
                        <th class="text-right" colspan="2"><h5><b>&#x20b9; <?php echo $data->net_amt;?></b></h5></th>
                      </tr>
					  <!--<tr>
                        <th class="text-left" colspan="5">Amount Chargeable (in words) : <b id="inwords"></b></th>
                      </tr>-->
					</tfoot>
                  </table>
                </div>
				<div class="table-responsive" style="clear: both;">
					<table class="table table-hover table-bordered">					  
					  <tr>
						<th class="text-center">Payment Method : <b><?php echo @$data->pay_method;?></b></th>
						<th class="text-center">Payment Status : <b><?php echo (@$data->pay_status == '1') ? 'Received' : 'Processing';?></b></th>
						<th class="text-center">UTR/Cheque No. : <b><?php echo @$data->pay_no;?></b></th>
						<th class="text-center" colspan="2">Bank Name : <b><?php echo @$data->bank_name;?></b></th>
					  </tr>
					</table>
                </div>
				<div class="pull-left">
                  <address>
					  <h5> &nbsp;<b class="text-danger">Company's Bank details</b></h5>
					  <p class="text-muted font-bold">
						Bank Name : AXIS BANK LTD. - 7451 <br/>
						A/C No. : 917020078037451 <br/>
						Branch & IFSC - Ahmednagar & UTIB0000215
					  </p>
                  </address>
                </div>
				<div  class="pull-right text-right">
                  <address>
                  <h5 class="font-bold">For Hope Technologies,</h5>
                  <p class="text-muted">Authorised Signatory</p>
                  </address>
                </div>
				<table class="table">
				  <tr>
					<td class="text-center text-muted">This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at advt@letsup.in</td>
				  </tr>
				</table>
              </div>
          </div>
        </div>
      </div>
      <!-- .row -->
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
    $(document).ready(function(){
        $("#print").click(function(){
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = { mode : mode, popClose : close};
            $("div.printableArea").printArea( options );
        });
		
		// American Numbering System
		var th = ['','thousand','million', 'billion','trillion'];
		var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; 
		function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}
		// Convert numbers to words
		// $("div").hover(function(){
			$('#inwords').html(toWords(<?php echo $data->net_amt;?>) + 'Only');                   
		// })
	});
  </script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
