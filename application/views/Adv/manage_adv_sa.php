<title><?php echo $title;?></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- Page plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<!-- Color picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
<!-- Date picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Select2 css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Popup CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<script>
	var tblexport = true;
	console.log('tblexport: '+tblexport);
</script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="row">
		<div class="col-lg-12 col-xs-12">
			<?php if($this->session->flashdata('success')){	?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
			<?php } if($this->session->flashdata('error')){	?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('error') ?>
				</div>
			<?php }	?>
		</div>
	</div>
      <div class="row">
		<div class="col-sm-12">
          <div class="white-box">
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>							
							<th>Status</th>
							<th>User Name</th>
							<th>Client</th>
							<th>Edition</th>>
							<th>Schedule Type</th>
							<th>Time</th>
							<th>Date</th>
							<th>Content</th>
							<th>Amt</th>
                            <th>Final Amt</th>
							<th>Remark</th>
							<th>Bank Name</th>
                            <th>Pay Method</th>
                            <th>Pay/Chq. No</th>
                            <th>Adv.Image</th>
                            <th>RO.Image</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Id</th>							
							<th>Status</th>
							<th>User Name</th>
							<th>Client</th>
							<th>Edition</th>>
							<th>Schedule Type</th>
							<th>Time</th>
							<th>Date</th>
							<th>Content</th>
							<th>Amt</th>
                            <th>Final Amt</th>
							<th>Remark</th>
							<th>Bank Name</th>
                            <th>Pay Method</th>
                            <th>Pay/Chq. No</th>
                            <th>Adv.Image</th>
                            <th>RO.Image</th>
                        </tr>						
                    </tfoot>
                    <tbody>
                        <?php $i = 1;
							if(count($data)>0){
								foreach($data as $result){ ?>
									<tr>
										<td><?php echo $i; ?></td>
                    <td> 
                        <select class="form-control" id="saflag<?php echo $i;?>"  onchange="changeSaFlag('<?php echo $result['id']; ?>',<?php echo $i;?>)">
                            <option <?php if ($result['saflag']== 1) echo 'selected' ; ?> value="1">Pending</option>
                            <option <?php if ($result['saflag']== 2) echo 'selected' ; ?> value="2">Approve</option>
                            <option <?php if ($result['saflag']== 3) echo 'selected' ; ?> value="3">Reject</option>
                        </select>															
                    </td>
										<td><?php echo $result['username']; ?></td>										
										<td><?php echo $result['business_name']; ?></td>
										<td>
											<?php $x= 0;
												$cities_id = explode(',',$result['city_id']);
												foreach($cities_id as $city_id){
													echo getCityName($cities_id[$x]).', '; 
													$x++;
												}
											?>
										</td>
										<td><?php echo $result['schedule_type']; ?></td>
										<td><?php echo $result['time']; ?></td>
										<td><?php echo date('d-M-y', strtotime($result['date'])); ?></td>
										<td><button type="submit" name="btn-view" id="btn-view" href="#test-form" class="popup-with-form btn-sm btn-info" onclick="viewContent('<?php echo $result['id']; ?>',<?php echo $i;?>,'Adv')">View</button></td>
										<td><?php echo $result['amt']; ?></td>
                                        <td><?php echo $result['net_amt']; ?></td>
                                        <td><?php echo $result['remark']; ?></td>
										<td><?php echo $result['bank_name']; ?></td>
                                        <td><?php echo $result['pay_method']; ?></td>
                                        <td><?php echo $result['pay_no']; ?></td>
                                        <td><a style="display:inline-block;color:#000;" title="img" target="_blank" href="<?php echo base_url();?>/uploads/admin/adv/<?php echo $result['image']; ?>" >Click</a></td>
                                        <td><a style="display:inline-block;color:#000;" title="img" target="_blank" href="<?php echo base_url();?>/uploads/admin/adv/<?php echo $result['ro_image']; ?>" >Click</a></td>
                                    </tr>
						<?php $i++; } } ?>                    
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
			<!-- form itself -->
				<div id="test-form" class="mfp-hide white-popup-block">
				  <fieldset style="border:0;">					
					  <div class="form-group">
						<button type="submit" class="btn btn-sm btn-info" onclick= "copyToClipboard('contentTextArea')">Copy</button>
						<br>
						<textarea rows="10" class="form-control" id="contentTextArea"></textarea>
					  </div>
				  </fieldset>
				</div>
			</div>
		</div>
	</div>
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- Custom Select2 JavaScript -->
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<!-- Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/moment/moment.js"></script>
<!-- Clock Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- Color Picker Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
jQuery(document).ready(function() {
    // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
    // For select 2

    $(".select2").select2();
    $('.selectpicker').selectpicker();
     
     //Bootstrap-TouchSpin
              $(".vertical-spin").TouchSpin({
                verticalbuttons: true,
                verticalupclass: 'ti-plus',
                verticaldownclass: 'ti-minus'
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
    
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
           
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
    
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
           
      // For multiselect

      $('#pre-selected-options').multiSelect();      
      $('#optgroup').multiSelect({ selectableOptgroup: true });

      $('#public-methods').multiSelect();
      $('#select-all').click(function(){
        $('#public-methods').multiSelect('select_all');
        return false;
      });
      $('#deselect-all').click(function(){
        $('#public-methods').multiSelect('deselect_all');
        return false;
      });
      $('#refresh').on('click', function(){
      $('#public-methods').multiSelect('refresh');
        return false;
      });
      $('#add-option').on('click', function(){
        $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
        return false;
      });
              
 });
 
// Clock pickers
$('#single-input').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'

});

$('.clockpicker').clockpicker({
    donetext: 'Done',
    
})
  .find('input').change(function(){
    console.log(this.value);
});

$('#check-minutes').click(function(e){
  // Have to stop propagation here
  e.stopPropagation();
  input.clockpicker('show')
      .clockpicker('toggleView', 'minutes');
});
if (/mobile/i.test(navigator.userAgent)) {
  $('input').prop('readOnly', true);
}
// Colorpicker

$(".colorpicker").asColorPicker();
$(".complex-colorpicker").asColorPicker({
    mode: 'complex'
});
$(".gradient-colorpicker").asColorPicker({
    mode: 'gradient'
});
// Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
      });
      
    jQuery('#date-range').datepicker({
        toggleActive: true
      });
    jQuery('#datepicker-inline').datepicker({
        
        todayHighlight: true
      });

// Daterange picker

$('.input-daterange-datepicker').daterangepicker({
  buttonClasses: ['btn', 'btn-sm'],
		applyClass: 'btn-danger',
		cancelClass: 'btn-inverse'
});
$('.input-daterange-timepicker').daterangepicker({
	timePicker: true,
	format: 'MM/DD/YYYY h:mm A',
	timePickerIncrement: 30,
	timePicker12Hour: true,
	timePickerSeconds: false,
	buttonClasses: ['btn', 'btn-sm'],
	applyClass: 'btn-danger',
	cancelClass: 'btn-inverse'
});
$('.input-limit-datepicker').daterangepicker({
	format: 'MM/DD/YYYY',
	minDate: '01/01/2019',
	maxDate: '02/01/2019',
	buttonClasses: ['btn', 'btn-sm'],
	applyClass: 'btn-danger',
	cancelClass: 'btn-inverse',
	dateLimit: {
		days: 6
	}
});
</script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/listing.js"></script>
<script src="<?php echo base_url();?>assets/js/common/adv.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>