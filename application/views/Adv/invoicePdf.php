<!DOCTYPE html>  
<html lang="en">
<head>
    <title><?php echo $title;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.ico">
    <script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
    <table border="1" width="100%" align="center" style="padding:10px 10px">
        <thead>
            <tr>
            <th ><h5><b>Invoice No: <?php echo '<br>'.$data->invoice_no;?></b></h5></th>
            <th ><h5><b>Invoice Date: <?php echo '<br>'.date('d-M-Y', strtotime($data->date));?></b></h5></th>
            <th ><h5><b>Supplier's Ref: <?php echo '<br>'.$data->supplier_ref;?></b></h5></th>
            <th ><h5><b>Executive Name: <?php echo '<br>'.$data->fname.' '.$data->lname;?></b></h5></th>
            <th ><h5><b>Ro.Code: <?php echo '<br>'.$data->id;?></b></h5></th>
            </tr>
        </thead>
    </table>
    <table width="100%" style="padding:10px 10px">
        <tr>
            <td>
                <address>
                <h3>To,</h3>
                <h4 ><?php echo $data->business_name;?>,</h4>
                <p ><?php echo $data->address;?></p>
                <p >GST No. <?php echo $data->gst_no;?></p>
                </address>
            </td>
            <td align="right">
                <address>
                    <p><img src="<?php echo base_url();?>assets/plugins/images/logo/letsup.png" alt="letsup" title="letsup" height="80px" alt="home" /></p>
                  <h4> &nbsp;<b>Hope Technologies</b></h4>
                  <p>144 Namoha Industries Compound, <br/>
                    Next to ST Depot Station Road, <br/>
                    Ahmednagar - 414001, <br/>
                    GSTIN/UIN: 27AAKFH6463J1ZX <br/>
                    Company PAN: AAKFH6463J <br/>
                    State Name: Maharashtra, Code :27
                  </p>
                </address>
            </td>
        </tr>
    </table>
                
    <table border="1" width="100%" style="padding:10px 10px">
        <thead>
            <tr>
                <th width="5%">#</th>
                <th width="55%">Description</th>
                <th width="15%">HSN/SAC</th>
                <th width="10%">%</th>
                <th width="15%">Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="5%">1</td>
                <td width="55%">
                    <?php echo $data->schedule_type;?><br>
                    Advertisement for <?php $x= 0;
                                            $cities_id = explode(',',$data->city_id);
                                            foreach($cities_id as $city_id){
                                                echo getCityName($cities_id[$x]).', '; 
                                                $x++;
                                            }
                                        ?> <br>
                    Time : <?php echo $data->timming;?><br>
                    Publish Date : <?php echo date('d-M-Y', strtotime($data->date));?>
                </td>
                <td width="15%">998366 </td>
                <td width="10%"></td>
                <td width="15%"> <?php echo $data->amt;?> </td>
            </tr>
            <tr align="right">
                <td colspan="2">Output CGST </td>
                <td>  </td>
                <td> 9% </td>
                <td align="center"><?php echo ($data->net_amt - $data->amt) / 2;?></td>
            </tr>
            <tr align="right">
                <td colspan="2">Output SGST </td>
                <td>  </td>
                <td> 9% </td>
                <td align="center"><?php echo ($data->net_amt - $data->amt) / 2;?></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2">Amount Chargeable (in words): <b><?php echo $data->net_amt_words; ?></b></td>
                <td align="center"><h5><b>Total :</b></h5></td>
                <td colspan="2" align="right"><h5><b><?php echo $data->net_amt;?></b></h5></td>
            </tr>
        </tfoot>
    </table>

    <table border="1" width="100%" style="padding:10px 10px" align="center">					  
        <tr>
            <td>Payment Method : <b><?php echo @$data->pay_method;?></b></td>
            <td colspan="2">Payment Status : <b><?php echo (@$data->pay_status == '1') ? 'Received' : 'Processing';?></b></td>
            <td>UTR/Cheque No. : <b><?php echo @$data->pay_no;?></b></td>
            <td>Bank Name : <b><?php echo @$data->bank_name;?></b></td>
        </tr>
    </table>

    <table width="100%" style="padding:10px 10px">
        <tr>
            <td>
                <address>
                    <h5> &nbsp;<b class="text-danger">Company's Bank details</b></h5>
                    <p class="text-muted font-bold">Bank Name : AXIS BANK LTD. - 7451 <br/>
                    A/C No. : 917020078037451 <br/>
                    Branch & IFSC - Ahmednagar & UTIB0000215
                    </p>
                </address>
            </td>
            <td align="right">
                <address>
                    <h5 class="font-bold">For Hope Technologies,</h5>
                    <p class="text-muted">Authorised Signatory</p>
                </address>
            </td>
        </tr>
    </table>
    <hr>
    <table width="100%">
        <tr>
            <td>This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at advt@letsup.in</td>
        </tr>
    </table>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
</body>
</html>
