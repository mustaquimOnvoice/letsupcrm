<!DOCTYPE html>  
<html lang="en">
<head>
<title><?php echo $title;?></title>
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>

    <table border="1" width="100%">
        <thead>
            <tr>
            <th ><h5><b>Invoice No: <?php echo '<br>'.$data->invoice_no;?></b></h5></th>
            <th ><h5><b>Invoice Date: <?php echo '<br>'.date('d-M-Y', strtotime($data->date));?></b></h5></th>
            <th ><h5><b>Supplier's Ref: <?php echo '<br>'.$data->supplier_ref;?></b></h5></th>
            <th ><h5><b>Executive Name: <?php echo '<br>'.$data->fname.' '.$data->lname;?></b></h5></th>
            <th ><h5><b>Ro.Code: <?php echo '<br>'.$data->id;?></b></h5></th>
            </tr>
        </thead>
    </table>
    <table border="1" width="100%">
        <tr>
            <td>
                <address>
                <h3>To,</h3>
                <h4 ><?php echo $data->business_name;?>,</h4>
                <p >GST No. <?php echo $data->gst_no;?></p>
                <p ><?php echo $data->address;?></p>
                </address>
            </td>
            <td>
                <address>
                    <p><img src="<?php echo base_url();?>assets/plugins/images/logo/letsup.png" alt="letsup" title="letsup" height="80px" alt="home" /></p>
                  <h4> &nbsp;<b>Hope Technologies</b></h4>
                  <p>144 Namoha Industries Compound, <br/>
                    Next to ST Depot Station Road, <br/>
                    Ahmednagar - 414001, <br/>
                    GSTIN/UIN: 27AAKFH6463J1ZX <br/>
                    Company PAN: AAKFH6463J <br/>
                    State Name: Maharashtra, Code :27
                  </p>
                </address>
            </td>
        </tr>
    </table>
                
    <table border="1" width="100%">
    <thead>
        <tr>
        <th >#</th>
        <th>Description</th>
        <th >HSN/SAC</th>
        <th >%</th>
        <th >Amount</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td >1</td>
        <td><?php echo $data->schedule_type;?>
        <p>
            Advertisement for <?php $x= 0;
                                    $cities_id = explode(',',$data->city_id);
                                    foreach($cities_id as $city_id){
                                        echo getCityName($cities_id[$x]).', '; 
                                        $x++;
                                    }
                                ?> <br/>
            Time : <?php echo $data->timming;?><br>
            Publish Date : <?php echo date('d-M-Y', strtotime($data->date));?>
        </p>
        </td>
        <td>998366 </td>
        <td>  </td>
        <td> <?php echo $data->amt;?> </td>
        </tr>
        <tr>
        <td colspan="2">OUTPUT CGST </td>
        <td>  </td>
        <td> 9% </td>
        <td> <?php echo ($data->net_amt - $data->amt) / 2;?> </td>
        </tr>
                <tr>
        <td colspan="2">OUTPUT SGST </td>
        <td>  </td>
        <td> 9% </td>
        <td> <?php echo ($data->net_amt - $data->amt) / 2;?> </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
        <th colspan="2">Amount Chargeable (in words): <b><?php echo $data->net_amt_words; ?></b></th>
        <th><h5><b>Total :</b></h5></th>
        <th colspan="2"><h5><b>&#x20b9; <?php echo $data->net_amt;?></b></h5></th>
        </tr>
    </tfoot>
    </table>

    <table border="1" width="100%">					  
        <tr>
        <th>Payment Method : <b><?php echo @$data->pay_method;?></b></th>
        <th>Payment Status : <b><?php echo (@$data->pay_status == '1') ? 'Received' : 'Processing';?></b></th>
        <th>UTR/Cheque No. : <b><?php echo @$data->pay_no;?></b></th>
        <th colspan="2">Bank Name : <b><?php echo @$data->bank_name;?></b></th>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr>
            <td>
                <address>
                    <h5> &nbsp;<b class="text-danger">Company's Bank details</b></h5>
                    <p class="text-muted font-bold">
                    Bank Name : AXIS BANK LTD. - 7451 <br/>
                    A/C No. : 917020078037451 <br/>
                    Branch & IFSC - Ahmednagar & UTIB0000215
                    </p>
                </address>
            </td>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr>
            <td>
                <address>
                <h5 class="font-bold">For Hope Technologies,</h5>
                <p class="text-muted">Authorised Signatory</p>
                </address>
            </td>
        </tr>
    </table>
    <table border="1" width="100%">
        <tr>
            <td>This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at advt@letsup.in</td>
        </tr>
    </table>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
    $(document).ready(function(){
        $("#print").click(function(){
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = { mode : mode, popClose : close};
            $("div.printableArea").printArea( options );
        });
		
		// American Numbering System
		var th = ['','thousand','million', 'billion','trillion'];
		var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; 
		function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}
		// Convert numbers to words
		// $("div").hover(function(){
			$('#inwords').html(toWords(<?php echo $data->net_amt;?>) + 'Only');                   
		// })
	});
  </script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
</body>
</html>
