<!DOCTYPE html>  
<html lang="en">
<head>
    <title><?php echo $title;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.ico">
    <script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
    <table width="100%" style="padding:10px 10px">
        <tr>
            <td>
                <p>
                    To,<br>
                    <?php echo $data->clientName;?>,<br>
                    <?php echo $data->business_name;?><br>
                    <?php echo $data->address;?><br><br>
                    Date: <?php echo date('d-M-Y');?>
                </p>
                <p style="font-family: Arial, Helvetica, sans-serif;">
                    <b style="font-family: Arial, Helvetica, sans-serif;">Respected Sir/Madam,</b><br>
                    Greetings from LetsUp – HOPE Technologies.<br><br>
                    We're delighted to inform you that your advertisement has been  published on (Date: <?php echo date('d-M-Y',strtotime($data->date));?>) (Time : <?php echo $data->timming;?> ) for (<?php echo $data->cityName;?>) in Marathi as per the scheduled booked by your approval.
                    We hope that this advertisement give your business a boost in the market and we wish you success and best wishes for the same.
                    <br><br>
                    You're our valuable customer & we look forward to have more mutual beneficiary association with your esteemed business.
                    Please feel free to reach out is if you have any queries or feedback at advt@letsup.in<br>
                    Please find the attached invoice of the transaction.<br>
                    Best Regards,<br>
                    LetsUp team
                </p>
            </td>
        </tr>
    </table>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.PrintArea.js" type="text/JavaScript"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
</body>
</html>
