<title>Edit Advertisement</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- page CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/bower_components/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Add Advertisement</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- .row -->
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo base_url(); ?>index.php/adv/edit_adv/?id=<?php echo base64_encode($data[0]['id']); ?>&status=<?php echo base64_encode($status); ?>" enctype="multipart/form-data" data-toggle="validator">
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="client_id" class="control-label">Client</label>
					<select class="form-control select2" id="client_id" name="client_id" required>
						<option value="">Select</option>
						<?php foreach($clients_details as $client){?>
							<option <?php echo ($data[0]['client_id'] == $client['id']) ? 'selected' : ''; ?> value="<?php echo $client['id'];?>"><?php echo $client['business_name'];?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"><?php if(form_error('client_id')!=""){ echo form_error('client_id');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="date" class="control-label">Date(mm/dd/yy)</label>
					<input type="date" class="form-control" id="date" name="date" value="<?php echo $data[0]['date'];?>" data-error="Date is required" required>
					<input type="hidden" class="form-control" id="ro_no" name="ro_no" placeholder="Ro.No" value="<?php echo $data[0]['ro_no']; ?>">
					<div class="help-block with-errors"><?php if(form_error('date')!=""){ echo form_error('date');} ?></div>
				  </div>
				  <!--<div class="form-group col-sm-3">
					<label for="ro_no" class="control-label">Ro.No.</label>
					<input type="text" class="form-control" id="ro_no" name="ro_no" placeholder="Ro.No" value="<?php echo $data[0]['ro_no']; ?>">
					<div class="help-block with-errors"><?php //if(form_error('ro_no')!=""){ echo form_error('ro_no');} ?></div>
				  </div>-->
				  <div class="form-group col-sm-3">
					<label for="schedule_type" class="control-label">Schedule Type</label>
					<select class="form-control" id="schedule_type" name="schedule_type" data-error="Schedule Type required" required>
						<option <?php echo ($data[0]['schedule_type'] == 'Full Screen') ? 'selected' : ''; ?> value="Full Screen">Full Screen</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Text') ? 'selected' : ''; ?> value="Text">Text</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Teaser') ? 'selected' : ''; ?> value="Teaser">Teaser</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Audio') ? 'selected' : ''; ?> value="Audio">Audio</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Video') ? 'selected' : ''; ?> value="Video">Video</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Club') ? 'selected' : ''; ?> value="Club">Club</option>
						<option <?php echo ($data[0]['schedule_type'] == 'Status') ? 'selected' : ''; ?> value="Status">Status</option>
					</select>
					<div class="help-block with-errors"><?php if(form_error('schedule_type')!=""){ echo form_error('schedule_type');} ?></div>
				  </div>
			  </div>
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="time" class="control-label">Time</label>
					<select class="form-control" id="time" name="time" data-error="Time required" required>
						<option value="">Select</option>
						<option <?php echo ($data[0]['time'] == '6 am') ? 'selected' : ''; ?> value="6 am">6 am</option>
						<option <?php echo ($data[0]['time'] == '8 am Club') ? 'selected' : ''; ?> value="8 am Club">8 am Club</option>
						<option <?php echo ($data[0]['time'] == '9:30 am') ? 'selected' : ''; ?> value="9:30 am">9:30 am</option>
						<option <?php echo ($data[0]['time'] == '11 am') ? 'selected' : ''; ?> value="11 am">11 am</option>
						<option <?php echo ($data[0]['time'] == '1 pm') ? 'selected' : ''; ?> value="1 pm">1 pm</option>
						<option <?php echo ($data[0]['time'] == '4:30 pm') ? 'selected' : ''; ?> value="4:30 pm">4:30 pm</option>
						<option <?php echo ($data[0]['time'] == '6:30 pm') ? 'selected' : ''; ?> value="6:30 pm">6:30 pm</option>
						<option <?php echo ($data[0]['time'] == '9 pm') ? 'selected' : ''; ?> value="9 pm">9 pm</option>
						<?php if($this->session->userdata('login_type') == 'Admin'){ ?>
							<option <?php echo ($data[0]['time'] == 'Custom') ? 'selected' : ''; ?> value="Custom">Custom</option>
						<?php }?>
					</select>
					<div class="help-block with-errors"><?php if(form_error('time')!=""){ echo form_error('time');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="city_id" class="control-label">Edition</label>
					<?php $arr_cities = explode(',',$data[0]['city_id']); ?>
					<select class="select2 m-b-10 select2-multiple" id="city_id" name="city_id[]" data-placeholder="Choose" data-error="Edition required" required  multiple="multiple">
						<?php foreach($citys_details as $city){?>
							<option <?php echo (in_array($city['id'],$arr_cities)) ? 'selected' : ''; ?> value="<?php echo $city['id'];?>"><?php echo $city['name'];?></option>
						<?php } ?>
					</select>
					<div class="help-block with-errors"><?php if(form_error('city_id')!=""){ echo form_error('city_id');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="contact" class="control-label">Rate</label>
					<div class="clearfix" id="demobox">
						<?php $rates = getCityRates($data[0]['id']);
							$x=0;
							foreach($rates as $rate){?>
							<input type="number" class="form-control city_rate" id="city_rate" name="city_rate[]" placeholder="" value="<?php echo $rates[$x++]['city_rate'];?>" required >
						<?php }?>
					</div>
					<input type="number" class="form-control" id="rate" name="rate" placeholder="Total Rate" value="<?php echo $data[0]['rate']; ?>" data-error="Rate required" required>
					<div class="help-block with-errors"><?php if(form_error('rate')!=""){ echo form_error('rate');} ?></div>
				  </div>
              </div>
			  <div class="row">
				  <div class="form-group col-sm-6">
					<label for="content" class="control-label">Content</label>
					<textarea rows="6" name="content" id="content" class="form-control" ><?php echo $data[0]['content']; ?></textarea>
					<div class="help-block with-errors"><?php if(form_error('content')!=""){ echo form_error('content');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="image" class="control-label">Adv. Image</label>
					<input type="hidden" name="image1" id="image1" value="<?php echo $data[0]['image']; ?>"/>
					<input type="file" class="form-control" id="image" name="image" value="<?php echo set_value('image');?>">
					<div class="help-block with-errors"><?php if(form_error('image')!=""){ echo form_error('image');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="ro_image" class="control-label">Ro. Image</label>
					<input type="hidden" name="ro_image1" id="ro_image1" value="<?php echo $data[0]['ro_image']; ?>"/>
					<input type="file" class="form-control" id="ro_image" name="ro_image" value="<?php echo set_value('ro_image');?>">
					<div class="help-block with-errors"><?php if(form_error('ro_image')!=""){ echo form_error('ro_image');} ?></div>
				  </div>
              </div>
			  <hr>
			  <div class="row">
				<h4>Payments Details :-</h4>
				  <div class="form-group col-sm-3">
					<label for="pay_method" class="control-label">Payment Method</label>
					<select class="form-control" id="pay_method" name="pay_method" data-error="Payment Method required" required>
						<option value="">Select</option>
						<option <?php echo ($data[0]['pay_method'] == 'Cash') ? 'selected' : ''; ?> value="Cash">Cash</option>
						<option <?php echo ($data[0]['pay_method'] == 'Online') ? 'selected' : ''; ?> value="Online">Online</option>
						<option <?php echo ($data[0]['pay_method'] == 'Cheque') ? 'selected' : ''; ?> value="Cheque">Cheque</option>
					</select>
					<div class="help-block with-errors"><?php if(form_error('pay_method')!=""){ echo form_error('pay_method');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="pay_status" class="control-label">Payment Status</label>
					<select class="form-control" id="pay_status" name="pay_status" data-error="Payment Status required" required>
						<option value="">Select</option>
						<option <?php echo ($data[0]['pay_status'] == '0') ? 'selected' : ''; ?> value="0">Pending</option>
						<option <?php echo ($data[0]['pay_status'] == '1') ? 'selected' : ''; ?> value="1">Received</option>
					</select>
					<div class="help-block with-errors"><?php if(form_error('pay_status')!=""){ echo form_error('pay_status');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="pay_status" class="control-label">UTR/Cheque No.</label>
					<input type="text" class="form-control" id="pay_no" name="pay_no" placeholder="UTR/Cheque No." value="<?php echo $data[0]['pay_no']; ?>">
					<div class="help-block with-errors"><?php if(form_error('pay_no')!=""){ echo form_error('pay_no');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="bank_name" class="control-label">Bank Name</label>
					<input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank name" value="<?php echo $data[0]['bank_name']; ?>">
					<div class="help-block with-errors"><?php if(form_error('bank_name')!=""){ echo form_error('bank_name');} ?></div>
				  </div>
              </div>
			  <div class="row">
				<div class="form-group col-sm-3">
					<label for="amt" class="control-label">Amount</label>
					<input type="number" step="any" class="form-control" id="amt" name="amt" placeholder="Amount" value="<?php echo $data[0]['amt']; ?>" data-error="Amount is required" required>
					<div class="help-block with-errors"><?php if(form_error('amt')!=""){ echo form_error('amt');} ?></div>
				</div>
				<div class="form-group col-sm-3">
					<label for="net_amt" class="control-label">Final Amount (GST+Amount)</label>
					<input type="number" step="any" class="form-control" id="net_amt" name="net_amt" placeholder="Amount" value="<?php echo $data[0]['net_amt']; ?>" data-error="Final Amount is required" required>
					<div class="help-block with-errors"><?php if(form_error('net_amt')!=""){ echo form_error('net_amt');} ?></div>
				</div>
				<div class="form-group col-sm-3">
					<label for="remark" class="control-label">Remark</label>
					<input type="text" class="form-control" id="remark" name="remark" placeholder="Remark" value="<?php echo $data[0]['remark']; ?>" data-error="Remark is required" required>
					<div class="help-block with-errors"><?php if(form_error('remark')!=""){ echo form_error('remark');} ?></div>
				</div>
              </div>
              <div class="form-group">
				<div class="row">
                  <div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/adv-edit.js"></script>
<script>
 jQuery(document).ready(function() {
    // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
    // For select 2

    $(".select2").select2();
    $('.selectpicker').selectpicker();
     
     //Bootstrap-TouchSpin
              $(".vertical-spin").TouchSpin({
                verticalbuttons: true,
                verticalupclass: 'ti-plus',
                verticaldownclass: 'ti-minus'
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
    
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
           
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
    
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
           
      // For multiselect

      $('#pre-selected-options').multiSelect();      
      $('#optgroup').multiSelect({ selectableOptgroup: true });

      $('#public-methods').multiSelect();
      $('#select-all').click(function(){
        $('#public-methods').multiSelect('select_all');
        return false;
      });
      $('#deselect-all').click(function(){
        $('#public-methods').multiSelect('deselect_all');
        return false;
      });
      $('#refresh').on('click', function(){
      $('#public-methods').multiSelect('refresh');
        return false;
      });
      $('#add-option').on('click', function(){
        $('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
        return false;
      });
              
 });

 </script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
