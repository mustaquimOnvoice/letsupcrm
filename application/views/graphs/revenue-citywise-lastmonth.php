
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueCitywiseLastMonth);
      function revenueCitywiseLastMonth() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueCitywiseLastMonth')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Cities Revenue <?php echo date_create(date('Y-m-d').'first day of last month')->format('F Y');?>',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('revenueCitywiseLastMonth'));
        chart.draw(data, options);
      }
    </script>
    <div id="revenueCitywiseLastMonth" style="width: 100%; height: 400px;"></div>