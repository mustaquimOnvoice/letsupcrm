
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueAprToMar);
      function revenueAprToMar() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueMarketingAprToMar')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Marketing users Financial year '+<?php echo $data['fyear']?>+'-'+<?php echo $data['toyear']?>,
          is3D: true,
        };

        var chart = new google.visualization.LineChart(document.getElementById('revenueMarketingAprToMar'));
        chart.draw(data, options);
      }
    </script>
    <hr>
    <div id="revenueMarketingAprToMar" style="width: 100%; height: 400px;"></div>