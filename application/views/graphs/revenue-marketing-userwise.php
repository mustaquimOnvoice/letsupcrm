
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueMarketingUserwise')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Marketing users total Revenue'
        };

        var chart = new google.visualization.LineChart(document.getElementById('revenueMarketingUserwise'));
        chart.draw(data, options);
      }
    </script>
    <hr>
    <div id="revenueMarketingUserwise" style="width: 100%; height: 400px;"></div>