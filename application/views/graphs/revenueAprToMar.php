
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueAprToMar);
      function revenueAprToMar() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueAprToMar')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Financial year '+<?php echo $data['fyear']?>+'-'+<?php echo $data['toyear']?>,
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('revenueAprToMar'));
        chart.draw(data, options);
      }
    </script>
    <div id="revenueAprToMar" style="width: 100%; height: 400px;"></div>