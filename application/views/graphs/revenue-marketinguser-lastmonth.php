
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueMarketingUserLastMonth);
      function revenueMarketingUserLastMonth() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueMarketingUserLastMonth')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Marketing users Revenue <?php echo date_create(date('Y-m-d').'first day of last month')->format('F Y');?>',
          is3D: true,
        };

        var chart = new google.visualization.LineChart(document.getElementById('revenueMarketingUserLastMonth'));
        chart.draw(data, options);
      }
    </script>
    <hr>
    <div id="revenueMarketingUserLastMonth" style="width: 100%; height: 400px;"></div>