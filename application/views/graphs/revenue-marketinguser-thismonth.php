
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueMarketingUserThisMonth);
      function revenueMarketingUserThisMonth() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueMarketingUserThisMonth')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Marketing users Revenue <?php echo date('F Y')?>',
          is3D: true,
        };

        var chart = new google.visualization.LineChart(document.getElementById('revenueMarketingUserThisMonth'));
        chart.draw(data, options);
      }
    </script>
    <hr>
    <div id="revenueMarketingUserThisMonth" style="width: 100%; height: 400px;"></div>