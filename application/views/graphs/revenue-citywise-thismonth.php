
    <script type="text/javascript">
      google.charts.setOnLoadCallback(revenueCitywiseThisMonth);
      function revenueCitywiseThisMonth() {
        var jsonData = $.ajax({ 
          url: "<?php echo site_url('Admin/revenueCitywiseThisMonth')?>", 
            dataType: "json", 
            async: false 
          }).responseText;

        var data = new google.visualization.DataTable(jsonData);

        var options = {
          title: 'Cities Revenue <?php echo date('F Y')?>',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('revenueCitywiseThisMonth'));
        chart.draw(data, options);
      }
    </script>
    <div id="revenueCitywiseThisMonth" style="width: 100%; height: 400px;"></div>