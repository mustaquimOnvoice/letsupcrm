<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse slimscrollsidebar">
		<ul class="nav" id="side-menu">		
            <li><a href="<?php echo site_url($this->session->userdata('login_type').'/dashboard');?>">Dashboard</a></li>	
            <li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Advertisement<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="<?php echo site_url('Superadmin/adv');?>">Pending</a> </li>
                </ul>
            </li>
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->