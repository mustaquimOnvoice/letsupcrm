<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Preloader --
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<!-- Preloader	
<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div> -->
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse slimscrollsidebar">
		<ul class="nav" id="side-menu">
			<?php if($this->session->userdata('login_type') != 'Watcher'){?>
				<li><a href="<?php echo site_url($this->session->userdata('login_type').'/dashboard/');?>">Dashboard</a></li>
			<?php }?>
			
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<li><a href="<?php echo site_url('Admin/business_cat');?>" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Business Category<span class="fa arrow"></span></span></a></li>
			<?php }?>
			
			<?php if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Marketing' || $this->session->userdata('login_type') == 'Account'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage Clients<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Client/add_client/');?>">Add Client</a> </li>
						<li> <a href="<?php echo site_url('Client/clients/');?>">View Clients</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage Users<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						 <li> <a href="<?php echo site_url('Admin/add_user/');?>">Add User</a> </li>
						<li> <a href="<?php echo site_url('Admin/users/');?>">View Users</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Marketing' || $this->session->userdata('login_type') == 'Operation'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage Advertisement<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<?php if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Marketing'){?>
							<li> <a href="<?php echo site_url('Adv/add_adv');?>">Schedule</a> </li>
						<?php }?>
						<li> <a href="<?php echo site_url('Adv/manage_adv/0');?>">Pending</a> </li>
						<li> <a href="<?php echo site_url('Adv/manage_adv/1');?>">Reject</a> </li>
						<li> <a href="<?php echo site_url('Adv/manage_adv/2');?>">Approved</a> </li>
						<li> <a href="<?php echo site_url('Adv/publish/3');?>">Publish</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Marketing' || $this->session->userdata('login_type') == 'Account'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Manage Invoice<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Adv/manageInvoice/3/invoice/2');?>">Pending</a> </li>
						<li> <a href="<?php echo site_url('Adv/manageInvoice/3/invoice/1');?>">Approved</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Revenue Reports<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Report/citywise');?>">City</a> </li>
						<li> <a href="<?php echo site_url('Report/catwise');?>">Category</a> </li>
					</ul>
				</li>
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin' || $this->session->userdata('login_type') == 'Marketing'){?>
			<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">DSR<span class="fa arrow"></span></span></a>
				<ul class="nav nav-second-level">
					<li> <a href="<?php echo site_url('dsr/add_dsr');?>">Add</a> </li>
					<li> <a href="<?php echo site_url('dsr/view_dsr');?>">View All</a> </li>
				</ul>
			</li>	
			<?php }?>
			<?php if($this->session->userdata('login_type') == 'Admin'){?>
				<li> <a href="javascript:void(0);" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Backup<span class="fa arrow"></span></span></a>
					<ul class="nav nav-second-level">
						<li> <a href="<?php echo site_url('Admin/backup');?>">Database</a> </li>
					</ul>
				</li>
			<?php }?>
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->