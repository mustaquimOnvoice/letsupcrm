<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.png">
    <title>Profile</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/default.css" id="theme" rel="stylesheet">
	<!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme" rel="stylesheet">
    
   
</head>

<body class="fix-sidebar">
    
    <div id="wrapper">
        <!-- Top Navigation -->
        <?php include 'header.php';?>
       <?php include 'navigation.php';?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Profile</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo site_url('Admin/dashboard/');?>">Dashboard</a></li>
							<li class="active"><a href="<?php echo site_url('Admin/profile/');?>">View Profile</a></li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav customtab nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> My Profile</span></a></li>
                                <li role="presentation" class="nav-item"><a href="#password" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Change Password</span></a></li>
                                
                            </ul>
                            <div class="tab-content">
								<?php
								foreach($profile_details as $rows)
								{
								?>
								<h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('success');?> </h4>
                                <div class="tab-pane active" id="profile">
										<form class="form-horizontal form-material" action="<?php echo site_url('Admin/edit_profile');?>" method="POST">
												<div class="form-group">
													<label class="col-md-12">Name</label>
													<div class="col-md-12">
														<input type="text" name="name" value="<?php echo $rows['name']?>" placeholder="Enter Name" class="form-control form-control-line">
													</div>
												</div>
												<div class="form-group">
													<label for="example-email" class="col-md-12">Mobile Number</label>
													<div class="col-md-12">
														<input type="number" placeholder="Enter Mobile Number" value="<?php echo $rows['mno']?>" class="form-control form-control-line" name="mno">
													</div>
												</div> 
												<div class="form-group">
													<label for="example-email" class="col-md-12">Email</label>
													<div class="col-md-12">
														<input type="email" style="text-trasnform:lowercase;" placeholder="Enter Email-Id" value="<?php echo $rows['email']?>" class="form-control form-control-line" name="email">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-12">Address</label>
													<div class="col-md-12">
														<textarea rows="5" name="address" placeholder="Address" class="form-control form-control-line"><?php echo $rows['address']?></textarea>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-12">
														<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Update Profile</button>
														<input type="hidden" name="user_id" value="<?php echo $rows['user_id']?>" placeholder="Enter Name" class="form-control form-control-line">
													</div>
												</div>
											
										</form>
                                </div>
                                <div class="tab-pane" id="password">
                                    <form data-toggle="validator" class="form-horizontal form-material" action="<?php echo site_url('Admin/change_password');?>" method="POST">
                                        <div class="form-group">
                                            <label class="col-md-12">Old Password</label>
                                            <div class="col-md-12">
                                                <input type="password" id="old_password" onkeyup="check_old_password();" placeholder="Old Password" name="old_password" class="form-control form-control-line">
												<p class="error3" style="display:none; color:green;"><i class="fa fa-check"></i> Password Match</p>
												<p class="error4" style="display:none; color:red;"><i class="fa fa-times"></i> Password Does Not Match</p>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-12">New Password</label>
                                            <div class="col-md-12">
                                                <input type="password" placeholder="New Password" id="new_password" name="new_password" class="form-control form-control-line">
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label class="col-md-12">Confirm Password</label>
                                            <div class="col-md-12">
                                                <input type="password" onkeyup="check();" id="confirm_password" placeholder="Confirm Password" name="confirm_password" class="form-control form-control-line">
												<p class="error" style="display:none; color:green;"><i class="fa fa-check"></i> Password Match</p>
												<p class="error1" style="display:none; color:red;"><i class="fa fa-times"></i> Password Does Not Match</p>
                                            </div>
                                        </div>
										<div class="form-group">
                                            <div class="col-md-12">
                                                <button disabled id="button" type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Change Password</button>
												<input type="hidden" id="password1" name="old-password" value="<?php echo $rows['password'];?>" class="form-control form-control-line">
												<input type="hidden" name="users_id" value="<?php echo $rows['user_id'];?>" class="form-control form-control-line">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php 
								}
								?>
                                
                            </div>
                        </div>
                    </div>
				
                </div>
                <!-- /.row -->
                <!-- .right-sidebar -->
              <?php include 'right-sidebar.php';?>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <?php include 'footer.php';?>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url();?>assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/validator.js"></script>
    <!--Style Switcher -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
		
function check_old_password()
		{

			var old_password = document.getElementById('old_password').value;
			var password1 = document.getElementById('password1').value;
			if(old_password==password1)
				{
					$('.error3').show();
					setTimeout(function() { $(".error3").hide(); },1000);
					return false;
				}
			else
				{
					$('.error4').show();
					setTimeout(function() { $(".error4").hide(); },1000);
					return false;
				}
		}
function check()
		{
			var old_password = document.getElementById('old_password').value;
			var password1 = document.getElementById('password1').value;
			
			var new_password = document.getElementById('new_password').value;
			var confirm_password = document.getElementById('confirm_password').value;
			
			if(new_password==confirm_password && old_password==password1)
				{
					$('.error').show();
					setTimeout(function() { $(".error").hide(); },1000);
					document.getElementById('button').disabled = false;
					return false;
				}
			else
				{
					$('.error1').show();
					setTimeout(function() { $(".error1").hide(); },1000);
					document.getElementById('button').disabled = true;
					return false;
				}
			
		}			
			
</script>
