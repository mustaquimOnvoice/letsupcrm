<?php
// echo "<pre>";
// print_r($file_details);
// die();
// echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.png">
    <title>Edit File</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bower_components/dropify/dist/css/dropify.min.css">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- color CSS -->
	<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>

<body class="fix-sidebar">
    
    <div id="wrapper">
        <!-- Top Navigation -->
        <?php include 'header.php';?>
        <?php include 'navigation.php';?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Edit File</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo site_url('/Admin/dashboard/');?>">Dashboard</a></li>
                            <li class="active">Edit File</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				<h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('success');?> </h4>
				<br>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
							<?php
							foreach($file_details as $rows)
							{
							?>
								<form action="<?php echo site_url('admin/update_file');?>" method="POST" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md-4">
											<h3 class="box-title">Select File</h3>
											<label for="input-file-now"><?php $file_name=$rows['file_name']; echo substr($file_name,0,-10);?></label>
											<input type="file" name="file" onchange="docValidation()" data-default-file="<?php $file_name=$rows['file_name']; echo substr($file_name,0,-10);?>" id="doc" class="dropify" accept=".zip,.Rar"/>
											<br>
											<p class="zip" style="display:none; color:red;"><b>Please Select only .Zip Or .Rar File Format.</b></p>
										</div>
										<div class="col-md-4">
											<h3 class="box-title">Select Category</h3>
											<label for="input-file-now"></label>
											<div class="form-group">
												<select name="category_id" class="form-control">
												<option value="<?php echo $rows['id'];?>"><?php echo $rows['category_name'];?></option>
													<?php
													foreach($category_details as $row)
													{
													?>
													<option value="<?php echo $row['id'];?>"><?php echo $row['category_name'];?></option>
													<?php
													}
													?>
												</select>
											</div>
										</div>
										
										<div class="col-md-4">
											<h3 class="box-title">Status</h3>
											<label for="input-file-now"></label>
											<div class="form-group">
												<select name="status" class="form-control">
													<option value=""><?php $status=$rows['status']; echo ($status==0 ? 'Enable':'Disable');?>
													</option>
													<option value="0">Enable</option>
													<option value="1">Disable</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<label for="input-file-now"></label>
											<div class="form-group">
												<button type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
												<input type="hidden" name="file_id"  class="form-control" value="<?php echo $rows['file_id'];?>" />
												<input type="hidden" name="old_file"  class="form-control" value="<?php echo $rows['file_name'];?>" />
												<input type="hidden" name="old_status"  class="form-control" value="<?php echo $rows['status'];?>" />
											</div>
										</div>
									</div>
								</form>
							<?php
							}
							?>
                    </div>

                </div>
                <!-- /.row -->
                
                
                <!-- /.row -->
                <!-- .right-sidebar -->
                <?php include 'right-sidebar.php';?>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <?php include 'footer.php';?>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url();?>assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jasny-bootstrap.js"></script>
    <!-- jQuery file upload -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
	
		setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
			
 
  //Upload File Only Zip
	function docValidation()
	{
		var fileInput = document.getElementById('doc');
		var filePath = fileInput.value;
		var allowedExtensions = /(\.zip|\.rar)$/i;
		if(!allowedExtensions.exec(filePath))
		{
			 $('.zip').show();
			setTimeout(function(){$('.zip').fadeOut('slow');}, 3000);
			fileInput.value = '';
		}
	}
	
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
    <!--Style Switcher -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>



</html>
