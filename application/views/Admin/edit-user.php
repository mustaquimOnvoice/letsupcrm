<title>Edit user</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
<?php //die('1');?>
</head>
<body>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit User</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo site_url(); ?>/admin/update_user/?id=<?php echo base64_encode($data->id); ?>" enctype="multipart/form-data" data-toggle="validator">
              <div class="form-group col-sm-3">
                <label for="first_name" class="control-label">First Name</label>
                <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" data-error="First Name is required" value="<?php echo $data->fname; ?>" required>
				<div class="help-block with-errors"><?php if(form_error('fname')!=""){ echo form_error('fname');} ?></div>
			  </div>
			  <div class="form-group col-sm-3">
                <label for="middle_name" class="control-label">Middle Name</label>
                <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" data-error="Middle Name is required" value="<?php echo $data->mname; ?>" required>
				<div class="help-block with-errors"><?php if(form_error('mname')!=""){ echo form_error('mname');} ?></div>
			  </div>
			  <div class="form-group col-sm-3">
                <label for="last_name" class="control-label">Last Name</label>
                <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" data-error="Last Name is required"  value="<?php echo $data->lname; ?>" required>
				<div class="help-block with-errors"><?php if(form_error('lname')!=""){ echo form_error('lname');} ?></div>
			  </div>
			  <div class="form-group col-sm-3">
                <label for="user_name" class="control-label">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="User Name" data-error="User Name is required"  value="<?php echo $data->username; ?>" required>
				<div class="help-block with-errors"><?php if(form_error('username')!=""){ echo form_error('username');} ?></div>
			  </div>
              <div class="form-group col-sm-3">
                <label for="inputEmail" class="control-label">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-error="Bruh, that email address is invalid"  value="<?php echo $data->email; ?>" required>
                <div class="help-block with-errors"><?php if(form_error('email')!=""){ echo form_error('email');} ?></div>
              </div>
			  <div class="form-group col-sm-3">
                <label for="mobile" class="control-label">Mobile Number</label>
                <input type="number" class="form-control" id="contact" name="contact" placeholder="Mobile No." data-error="Mobile Number required" value="<?php echo $data->contact; ?>" required>
                <div class="help-block with-errors"><?php if(form_error('contact')!=""){ echo form_error('contact');} ?></div>
              </div>
			  <div class="form-group col-sm-3">
                <label for="user_type" class="control-label">User Type</label>
				<select class="form-control" id="type" name="type" data-error="User type required" required>
                    <option value="">Select</option>
                    <option <?php echo ($data->type == '1') ? 'selected' : ''; ?> value="1">Admin</option>
					<!--<option value="2">Marketing Head</option>-->
					<option <?php echo ($data->type == '3') ? 'selected' : ''; ?> value="3">Marketing</option>
					<option <?php echo ($data->type == '4') ? 'selected' : ''; ?> value="4">Accounts</option>
					<option <?php echo ($data->type == '5') ? 'selected' : ''; ?> value="5">Operation</option>
					<!--<option value="6">Analyzer</option>-->
                </select>
                <div class="help-block with-errors"><?php if(form_error('type')!=""){ echo form_error('type');} ?></div>
              </div>
			  <div class="form-group col-sm-3">
				<label for="status" class="control-label">Login Status</label>
                <div class="radio">
                  <input type="radio" name="status" id="out" <?php echo ($data->status == '1') ? 'checked' : ''; ?> value="1" required>
                  <label for="out"> Active </label>
                </div>
                <div class="radio">
                  <input type="radio" name="status" id="in"  <?php echo ($data->status == '0') ? 'checked' : ''; ?> value="0" required>
                  <label for="in"> Inactive </label>
                </div>
				<div class="help-block with-errors"><?php if(form_error('status')!=""){ echo form_error('status');} ?></div>
              </div>
              <div class="form-group col-sm-12">
                <label for="inputPassword" class="control-label">New Password</label>
                <div class="row">
                  <div class="form-group col-sm-3">
					<input type="hidden" class="form-control" id="oldpassword" name="oldpassword" placeholder="Password" value="<?php echo $data->password; ?>">
                    <input type="password" data-toggle="validator" class="form-control" data-minlength="6" name="inputPassword" id="inputPassword" placeholder="Password">
                    <span class="help-block">Minimum of 6 characters</span> </div>
                  <div class="form-group col-sm-3">
                    <input type="password" class="form-control"  name="inputPasswordConfirm" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password">
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>             
              <div class="form-group">
				<div class="row">
                  <div class="form-group col-sm-3">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php// echo base_url();?>assets/js/admin/users.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
