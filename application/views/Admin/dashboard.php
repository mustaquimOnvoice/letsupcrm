
<title>Dashboard</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- morris CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="<?php // echo base_url();?>assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="<?php // echo base_url();?>assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
  // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  // })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

  // ga('create', 'UA-19175540-9', 'auto');
  // ga('send', 'pageview');

</script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php echo $header;?>
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
       <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Dashboard</h4>
        </div>
         <!--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <a href="#" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard </li>
          </ol>
        </div>-->
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">        
		<div class="col-md-12 col-lg-12 col-sm-12">
          <div class="white-box">
			<div class="row row-in">
			  <a href="<?php echo site_url('Adv/add_adv')?>" title="Schedule Advertisement">
				  <div class="col-lg-2 col-sm-6 row-in-br">
					<div class="col-in row">
					  <div class="col-md-6 col-sm-6 col-xs-6"><i class="fa fa-plus-circle" aria-hidden="true"></i>
						<h5 class="text-muted vb">SCHEDULE ADVERTISEMENT</h5>
					  </div>
					  <!--<div class="col-md-6 col-sm-6 col-xs-6">
						<a href="<?php // echo site_url('Adv/add_adv')?>"><h3 class="counter text-right m-t-15 text-success"><?php // echo $total_publish_adv;?></h3></a>
					  </div>-->
					  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
						</div>
					  </div>
					</div>
				  </div>
			  </a>
			  <a href="<?php echo site_url('Adv/manage_adv/0')?>" title="Advertisement Pending">
				  <div class="col-lg-2 col-sm-6 row-in-br">
					<div class="col-in row">
					  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-files-o" aria-hidden="true"></i>
						<h5 class="text-muted vb">ADVERTISEMENT PENDING</h5>
					  </div>
					  <div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-danger"><?php echo $total_pending_adv;?></h3>
					  </div>
					  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
						</div>
					  </div>
					</div>
				  </div>
			  </a>
			  <a href="<?php echo site_url('Adv/manage_adv/1')?>" title="Advertisement Reject">
				  <div class="col-lg-2 col-sm-6 row-in-br">
					<div class="col-in row">
					  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-times" aria-hidden="true"></i>
						<h5 class="text-muted vb">ADVERTISEMENT REJECT</h5>
					  </div>
					  <div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-danger"><?php echo $total_rejected_adv;?></h3>
					  </div>
					  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
						</div>
					  </div>
					</div>
				  </div>
			  </a>
			  <a href="<?php echo site_url('Adv/manage_adv/2')?>" title="Advertisement Approved">
				  <div class="col-lg-2 col-sm-6 row-in-br">
					<div class="col-in row">
					  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-check" aria-hidden="true"></i>
						<h5 class="text-muted vb">ADVERTISEMENT APPROVED</h5>
					  </div>
					  <div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-megna"><?php echo $total_approved_adv;?></h3>
					  </div>
					  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
						</div>
					  </div>
					</div>
				  </div>
			  </a>
			  <a href="<?php echo site_url('Adv/publish/3')?>" title="Advertisement Publish">
				  <div class="col-lg-4 col-sm-6 row-in-br">
					<div class="col-in row">
					  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-star" aria-hidden="true"></i>
						<h5 class="text-muted vb">ADVERTISEMENT PUBLISH</h5>
					  </div>
					  <div class="col-md-6 col-sm-6 col-xs-6">
						<h3 class="counter text-right m-t-15 text-success"><?php echo $total_publish_adv;?></h3>
					  </div>
					  <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
						</div>
					  </div>
					</div>
				  </div>
			  </a>
			</div>
          </div>
        </div>
		<div class="col-md-12 col-lg-12 col-sm-12">
          <div class="white-box">
            <div class="row row-in">
			<a href="<?php echo site_url('Client/clients/')?>" title="Total Clients">
              <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
				  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-users" aria-hidden="true"></i>
					<h5 class="text-muted vb">TOTAL CLIENTS</h5>
				  </div>				
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <a href="<?php echo site_url('Client/clients/')?>"><h3 class="counter text-right m-t-15 text-info"><?php echo $total_clients;?></h3></a>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div> <b>Regular :</span> <?php echo $regularClient;?></b> </div>
                    </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
                      <div><b>Non-Regular : <?php echo $total_clients - $regularClient;?></b> </div>
                    </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
                      <div><b>New Client:</span> <?php echo $newClientsInCurrentmonth;?></b> </div>
                    </div>
                  </div>
                </div>
              </div>
			</a>
			<a href="<?php echo site_url('Admin/users/')?>" title="Total Users">
			  <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-user" aria-hidden="true"></i>
                    <h5 class="text-muted vb">TOTAL USERS</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-info"><?php echo $total_users;?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
			</a>
			<a href="<?php echo site_url('Adv/manageInvoice/3/invoice/2')?>" title="Invoices Pending">
              <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-calendar-times-o" aria-hidden="true"></i>
                    <h5 class="text-muted vb">INVOICE PENDING</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-danger"><?php echo $total_pending_invoice;?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
			</a>
			<a href="<?php echo site_url('Adv/manageInvoice/3/invoice/1')?>" title="Invoices Approved">
              <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    <h5 class="text-muted vb">INVOICES APPROVED</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-primary"><?php echo $total_approved_invoice;?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
			  </div>
			</a>
			</div>
		  </div>
		</div>
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueCitywise;?>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueAprToMar;?>
    </div> 
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueCitywiseLastMonth;?>
    </div> 
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueCitywiseThisMonth;?>
    </div>    
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueMarketingUserwise;?>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueMarketingAprToMar;?>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueMarketingUserLastMonth;?>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12">
        <?php echo $revenueMarketingUserThisMonth;?>
    </div>
    </div>
  </div>
    <!-- /.container-fluid -->
     <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/raphael/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/morrisjs/morris.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>

<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
