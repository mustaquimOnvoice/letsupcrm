
<!DOCTYPE html>  
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/images/favicon.png">
<title>Add Category</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>

<div id="wrapper">
  <!-- Top Navigation -->
  
	<!-- End Top Navigation -->
	<!-- Left navbar-header -->
	<?php include 'header.php';?>
	<?php include 'navigation.php';?>
	<!-- Left navbar-header end -->
	<!-- Page Content -->
	<div id="page-wrapper">
	<div class="container-fluid">
	  <div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		  <h4 class="page-title">Add Category</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		  <ol class="breadcrumb">
			<li><a href="<?php echo site_url('/Admin/dashboard/');?>">Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('Admin/view_category/');?>">View Category</a></li>
		  </ol>
		</div>
		<!-- /.col-lg-12 -->
	  </div>
	  <h4 class="box-title m-b-0 text-center" style="color:#03a9f3;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('success');?> </h4>
	  <br>
	  <div class="row">
		<div class="col-md-6">
		  <div class="white-box">
			<h3 class="box-title m-b-0">Category Name</h3>
			<div class="row">
			  <div class="col-sm-12 col-xs-12">
				<form action="<?php echo site_url('Admin/insert_category');?>" method="POST"> 
				  <div class="form-group">
					<!--<label for="exampleInputEmail1">Category Name</label>-->
					<br>
					<input type="text" onkeyup="check();" id="cat_name" name="category_name" class="form-control" placeholder="Enter Category Name" autocomplete="off" />
					<p class="error1" style="display:none; color:red;"><i class="fa fa-times"></i> Enter Category Name</p>
				  </div>
				 <button disabled id="button" type="submit" name="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	   
	  </div>
	  <!-- .right-sidebar -->
	  <?php include 'right-sidebar.php';?>
	</div>
	<!-- /.container-fluid -->
	<?php include 'footer.php';?>
	</div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assetsjs/jasny-bootstrap.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
<script>
		setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
		
function check()
	{	
	var cat_name = document.getElementById("cat_name");
	if(cat_name.value!='') 
				{ 
					document.getElementById('button').disabled = false; 
				} 
				else 
				{ 
					document.getElementById('button').disabled = true;
				}
	var cat_name = document.getElementById("cat_name");
			if(category.value=='')
				{
					$('.error1').show();
					setTimeout(function() 
						{ 
							$(".error1").hide();
						}, 3000);
					return false;
				}
	
	}			
</script>