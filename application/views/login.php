<?php include 'Admin/head.php';?>
	<title>Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url();?>assets/css/colors/default.css" id="theme" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" id="loginform" method="POST" action="<?php echo site_url('login/check_login'); ?>">
				
				<a href="javascript:void(0)" class="text-center db"><img src="<?php echo base_url();?>assets/plugins/images/logo/letsup.png" alt="letsup" title="letsup" height="80px" alt="home" /></a>
				<br/> 
				<h4 class="box-title m-b-0 text-center" style="color:red;" onload ="return setTimeout();" id="timeout"><?php echo $this->session->flashdata('error');?> <?php echo $this->session->flashdata('suspend');?> </h4>				
				<h4 class="box-title m-b-0 text-center" style="color:green;" onload ="return setTimeout();" id="timeout1"><?php echo $this->session->flashdata('success');?> </h4>				
				   <h3 class="box-title m-b-20 text-center"><b>Sign In</b></h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input id="username" onkeyup="check();" style="text-transform:lowercase;" class="form-control" name="username" type="text" required="" placeholder="Username" autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="passwords" onkeyup="check();" class="form-control" name="password" type="password" required="" placeholder="Password" autocomplete="off" />
                        </div>
                    </div>
                   
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button id="button" disabled class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" name="submit" type="submit">Log In</button>
                        </div>
                    </div>
					
                 </form>
				
             </div>
        </div>
		
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url();?>assets/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
<script>
setTimeout(function() {
            $('#timeout').fadeToggle('slow');
            }, 3000);
	setTimeout(function() {
            $('#timeout1').fadeToggle('slow');
            }, 3000);
			
function check()
	{
			var user_name=document.getElementById('username').value;
			var pass_word=document.getElementById('passwords').value;
			
			if(user_name.length > 0 && pass_word.length > 0)
				{
					document.getElementById('button').disabled = false;
				}
				else
				{
					document.getElementById('button').disabled = true;
				}
	}	
</script>
