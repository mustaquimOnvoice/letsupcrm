<title>Edit Daily Sales Report</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url();?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets/www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Top Navigation -->
  <?php echo $header;?>
<?php //die('s');?>
  <!-- End Top Navigation -->
  <!-- Left navbar-header -->
  <?php echo $nav;?>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit Daily Sales Report</h4>
        </div>
        <!-- /.col-lg-12 -->
      </div>
  		<div class="row">
			<div class="col-lg-12 col-xs-12">
			<div id ="resultMsg">
			</div>
				<?php if($this->session->flashdata('success')){	?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('success') ?>
					</div>
				<?php } if($this->session->flashdata('error')){	?>
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php }	?>
			</div>
		</div>
      <!-- .row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <form method="post" name="edit-form" id="edit-form" action="<?php echo site_url(); ?>/dsr/update_dsr/?id=<?php echo base64_encode($data->id); ?>" enctype="multipart/form-data" data-toggle="validator" >
			  <div class="row">
				  <div class="form-group col-sm-3">
					<label for="name" class="control-label">Name</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Name" data-error="Name is required" value="<?php echo $data->name; ?>" required>
					<div class="help-block with-errors"><?php if(form_error('name')!=""){ echo form_error('name');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="business_name" class="control-label">Business Name</label>
					<input type="text" class="form-control" id="business_name" name="business_name" placeholder="Business Name" data-error="Business Name is required" value="<?php echo $data->business_name; ?>" required>
					<div class="help-block with-errors"><?php if(form_error('business_name')!=""){ echo form_error('business_name');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="email" class="control-label">Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $data->email; ?>" >
					<div class="help-block with-errors"><?php if(form_error('email')!=""){ echo form_error('email');} ?></div>
				  </div>
				  <div class="form-group col-sm-3">
					<label for="t_o_call" class="control-label">Type of Call</label>
					<select class="form-control select2" id="t_o_call" name="t_o_call" required>
						<option value=""><b>Select Type of call</b></option>
						<option <?php echo ($data->t_o_call == 'Cold') ? 'selected' : ''; ?> value="Cold">Cold</option>
						<option <?php echo ($data->t_o_call == 'Warm') ? 'selected' : ''; ?> value="Warm">Warm</option>
					</select>
					<div class="help-block with-errors"></div>
				  </div>
			  </div>
			  <div class="row">
				<div class="form-group col-sm-3">
					<label for="call_time" class="control-label">Call time</label>
					<input type="text" class="form-control" id="call_time" name="call_time" placeholder="Call time" data-error="GST No. is required" value="<?php echo $data->call_time; ?>" required>
					<div class="help-block with-errors"><?php if(form_error('call_time')!=""){ echo form_error('call_time');} ?></div>
				</div>
				  <div class="form-group col-sm-3">
					<label for="contact" class="control-label">Mobile Number</label>
					<input type="number" class="form-control" id="contact" name="contact" placeholder="Mobile No." data-error="Mobile Number required" value="<?php echo $data->mobile; ?>" required>
					<div class="help-block with-errors"><?php if(form_error('mobile')!=""){ echo form_error('mobile');} ?></div>
				  </div>
				<div class="form-group col-sm-6">
					<label for="address" class="control-label">Address</label>
					<input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo $data->address; ?>" >
					<div class="help-block with-errors"><?php if(form_error('address')!=""){ echo form_error('address');} ?></div>
				</div>
              </div>
			  <div class="row">
				<div class="form-group col-sm-3">
					<label for="date" class="control-label">Date</label>
					<input type="date" class="form-control" id="date" name="date" placeholder="date" value="<?php echo date("m/d/Y",strtotime($data->dsr_date));?>" >
					<input type="hidden" class="form-control" id="dsr_date" name="dsr_date" placeholder="date" value="<?php echo date("m/d/Y",strtotime($data->dsr_date));?>" >
					<div class="help-block with-errors"></div>
				</div>
				<div class="form-group col-sm-9">
					<label for="summary" class="control-label">Summary</label>
					<input type="text" class="form-control" id="summary" name="summary" placeholder="Summary" value="<?php echo $data->summary; ?>" >
					<div class="help-block with-errors"><?php if(form_error('summary')!=""){ echo form_error('summary');} ?></div>
				</div>
              </div>
              <div class="form-group">
				<div class="row">
                  <div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </div>
    <!-- /.container-fluid -->
    <?php echo $footer;?>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Admin/users Page Custome JS -->
<script src="<?php echo base_url();?>assets/js/admin/users.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
