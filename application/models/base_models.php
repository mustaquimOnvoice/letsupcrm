<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class base_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	function Custome_quary($quarys) {
		$querya = $this->db->query ( $quarys );
		return $querya->result_array ();
	}

	function CustomeQuary($quarys) {
		$querya = $this->db->query ( $quarys );
		return $querya->result_array ();
	}
	
	function AddValues($TableName, $TableValues) {
		$this->db->insert ( $TableName, $TableValues );
		$insert_id = $this->db->insert_id ();
		return $insert_id;
	}
	
	function RemoveValues($TableName, $wherecondition) {
		$this->db->where ( $wherecondition );
		$this->db->delete ( $TableName );
		return $this->db->affected_rows();
	}
	
	function UpadateValue($TableName, $data, $wherecondition) {
		$this->db->where ( $wherecondition );
		return $this->db->update ( $TableName, $data );
	}
	
	function CustomeUpdateQuary($quarys) {
		$querya = $this->db->query ( $quarys );
	}
	
	function Update_Custome_quary($quarys) {
		$querya = $this->db->query ( $quarys );
	}
	
	function GetAllValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		$querys = $this->db->get ();
		return $querys->result_array ();
	}
	
	function GetSingleDetails($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		$querys = $this->db->get ();
		return $querys->row();
	}
	
	public function sendSMS($to_mobileno, $sendmsg) {
        $sender = "SMARTX";
        $smslogid = "1";

        $url = 'http://api.myvaluefirst.com/psms/servlet/psms.Eservice2?';


        $xmlstring = '<?xml version="1.0" encoding="ISO-8859-1"?>
                    <!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1:80/psms/dtd/messagev12.dtd">
                    <MESSAGE VER="1.2">
                    <USER USERNAME="sundaytechllp" PASSWORD="stechvlp"/>
                    <SMS  UDH="0" CODING="1" TEXT="' . $sendmsg . '" PROPERTY="0" ID="' . $smslogid . '">
                    <ADDRESS FROM="' . $sender . '" TO="' . $to_mobileno . '" SEQ="1" TAG="some clientside random data"/>
                    </SMS>
                    </MESSAGE>';


        $data = 'data=' . urlencode($xmlstring) . '&' . 'action=send';

        $objURL = curl_init($url);

        curl_setopt($objURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($objURL, CURLOPT_POST, 1);
        curl_setopt($objURL, CURLOPT_POSTFIELDS, $data);
        $retval = trim(curl_exec($objURL));

        curl_close($objURL);
    }
	
	
	// ************************************************** Custome ***********************************************//
	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)){
			if ($this->db->insert($table_name,$insert_array))
				return true;
			else
				return false;
		}else{
			return false; 
		}

	}
	public function get_records($table_name,$filed_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($filed_name_array) && isset($filed_name_array)){
	  		$str=implode(',',$filed_name_array);
			$this->db->select($str);
		}	

		if(is_array($where_array)&& isset($where_array)){
			$this->db->where($where_array);
		}
		$result=$this->db->get($table_name);		

		if($single_result==true && isset($single_result)){
			return $result->row_array();
		}else{
			return $result->result_array();
		}
	}
	
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function delete_records($table_name,$where_array)
	{ 
		if (is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->delete($table_name))
				return true;
			else
				return false;
		}else{
			return false;
		}
	}
	//Users types 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	
	function count_users($type = NULL){
		$this->db->select('id')
			->from('wwc_admin')
			->where(array('status'=>'1'));
		if($type != NULL){
			$this->db->where(array('type' => $type));			
		}
		return $this->db->get()->num_rows();
	}
	
	function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', '2');
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }
	
	public function fetch_users($limit, $start) { 
       $this->db->limit($limit, $start); 
       $query = $this->db->get("wwc_admin"); 
       if ($query->num_rows() > 0) { 
           foreach ($query->result() as $row) { 
               $data[] = $row; 
           } 
           return $data; 
       } 
       return false; 
   }
	//End Users
		
	//Clients
	function count_clients(){
		return $this->db->select('id')
				->from('tbl_client')
				->where(array('status !='=>'2'))
				->get()
				->num_rows();
	}
	
	function get_clients($select = '', $id = '', $searchText = '', $page='', $segment='',$user_id='')
    {
        $this->db->select($select);
        $this->db->from('tbl_client');
        $this->db->join('business_cat','tbl_client.bcat = business_cat.id','Left');
        $this->db->join('wwc_admin','tbl_client.user_id = wwc_admin.id','Left');
        if(!empty($searchText)) {
            $likeCriteria = "(tbl_client.name  LIKE '%".$searchText."%'
                            OR  tbl_client.business_name  LIKE '%".$searchText."%'
                            OR  tbl_client.email  LIKE '%".$searchText."%'
                            OR  tbl_client.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('tbl_client.id'=>$id));
		}
		if(!empty($user_id)) {
			$this->db->where(array('tbl_client.user_id'=> $user_id ));
		}
        $this->db->where('tbl_client.status !=', '2');
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }
	
	public function fetch_clients($limit, $start) { 
       $this->db->limit($limit, $start); 
       $query = $this->db->get("tbl_client"); 
       if ($query->num_rows() > 0) { 
           foreach ($query->result() as $row) { 
               $data[] = $row; 
           } 
           return $data; 
       } 
       return false; 
	}
	//End Clients
		
	//Advertsiment types/status 0=pending, 1=rejected, 2=approved, 3=publish
	function count_adv($type = '0',$id = ''){
		$this->db->select('id')
				->from('tbl_adv')
				->where(array('status'=>$type));
			if($id != ''){
				$this->db->where(array('user_id'=>$id));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	
	function get_adv_data($status,$where){
		return $this->db->query(
						"SELECT ta.*, tcl.business_name as business_name, tcl.address as address, wd.username
						FROM tbl_adv as ta
						LEFT JOIN wwc_admin as wd
						ON wd.id = ta.user_id
						LEFT JOIN tbl_client as tcl
						ON tcl.id = ta.client_id
						WHERE ta.status = '$status' $where
						ORDER BY ta.id DESC"
						)->result_array();
	}

	function get_adv_data_pagi($status,$where,$limit,$start,$userlogin){
		$select	= array('ta.*', 'tcl.business_name as business_name','tcl.address as address','wd.username');	
		$this->db->select($select);
		$this->db->from('tbl_adv as ta');
		$this->db->join('wwc_admin as wd','ON wd.id = ta.user_id','Left');
		$this->db->join('tbl_client as tcl','ON tcl.id = ta.client_id','Left');
		$this->db->where(array('ta.status' => $status));
		if (!empty ( $userlogin )){
			$this->db->where ( $userlogin );
		}
		$this->db->order_by("ta.id", 'DESC');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		return $this->db->get()->result_array();
		
	}

	function count_adv_data($status,$where){
		return $this->db->query(
						"SELECT ta.id
						FROM tbl_adv as ta
						LEFT JOIN wwc_admin as wd
						ON wd.id = ta.user_id
						LEFT JOIN tbl_client as tcl
						ON tcl.id = ta.client_id
						WHERE ta.status = '$status' $where"
						)->num_rows();
	}
	//End Advertsiment
	
	//Daily Sales Report
	function get_dsr_data($where){
		return $this->db->query("SELECT td.*, wa.username
								FROM tbl_dsr as td
								LEFT JOIN wwc_admin as wa
								ON wa.id = td.added_by
								WHERE $where ORDER BY id DESC")->result();
	}
	//Daily Sales Report
	
	//Invoice types 1=approved, 2=pending
	function count_invoice($type = '1',$id = ''){
		$this->db->select('id')
				->from('tbl_adv')
				->where(array('status'=>'3', 'invoice_generate' => $type));
			if($id != ''){
				$this->db->where(array('user_id'=>$id));
			}
		$res = $this->db->get();
		return	$res->num_rows();
	}
	function get_invoice_details($id = ''){
		$select = array('td.id','td.user_id','td.client_id','td.city_id','td.ro_no','td.schedule_type','td.time as timming','td.date','td.bank_name','td.pay_method','td.pay_no','td.pay_status','td.amt','td.net_amt','td.net_amt','td.invoice_no','td.invoice_date','td.supplier_ref',
						'wa.fname','wa.lname','tc.gst_no','tc.name as clientName','tc.business_name','tc.address');		
		$where = array('td.id' => $id);
		$data = $this->db->select($select)
					->from('tbl_adv as td')
					->join('wwc_admin as wa','ON td.user_id = wa.id','Left')
					->join('tbl_client as tc','ON td.client_id = tc.id','Left')
					->where($where)
					->get()->row();
		return	$data;
	}
	//End Invoice

	//Watcher dashboard 
	function getTodaysBooking($time,$cityid,$date){
		return $this->db->query("SELECT tb.schedule_type, tc.business_name, wd.username, ta.status, ta.image
								FROM tbl_booking as tb
								LEFT JOIN tbl_adv as ta 
								ON tb.adv_id = ta.id
								LEFT JOIN tbl_client as tc
								ON tc.id = ta.client_id
								LEFT JOIN wwc_admin as wd
								ON wd.id = ta.user_id
								WHERE tb.city_id = $cityid
								AND tb.time = '$time'
								AND tb.date = '$date'
								AND (ta.status = '2' OR ta.status = '0')")->row();
	}

	//Admin Dashboard
	// count regular  client
	function regularClient(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d');
		$this->db->select(array('tc.id', 'tc.business_name', 'COUNT(ta.client_id) as regular'))
				->from('tbl_client as tc')
				->join('tbl_adv as ta','ON ta.client_id = tc.id','Left')
				->where(array('ta.date >='=> "$fdate", 'ta.date <='=> "$todate",'tc.status' => '1','ta.status !=' => '4'))
				->group_by('ta.client_id')
				->having(array('COUNT(ta.client_id) >' => '1'));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
	
	function nonRegularClient(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d');
		$this->db->select(array("tc.id", "tc.business_name",'(SELECT COUNT(client_id) from tbl_adv where client_id = tc.id AND date BETWEEN "$fdate" AND "$todate" AND status != "4") as nonregular'))
				->from('tbl_client as tc')
				->where(array('tc.status !=' => '2'))
				->having(array('nonregular <=' => '1'));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
	
	function newClientsInCurrentmonth(){
		$fdate = date('Y-m-01');
		$todate = date('Y-m-d',strtotime(date('Y-m-d') . ' +1 day'));
		$this->db->select(array("id"))
				->from('tbl_client')
				->where(array('created_at >='=> "$fdate", 'created_at <='=> "$todate"));
		$res = $this->db->get();
		return	$res->num_rows();
		//return	$res->result_array();
	}
}
?>