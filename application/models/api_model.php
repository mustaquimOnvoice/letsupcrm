<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class api_model extends CI_Model {
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	
	final function check_token($tableName, $user_mobile_no, $device_token){
		$TableValues['device_token'] = $device_token;
		$TableValues['user_mobile_no'] = $user_mobile_no;
		$temp = $this->Base_Models->GetAllValues ( $tableName, $TableValues, array('device_token'));
		
		if(count($temp)>0){
			return true;
		}
		$response ['message'] = "fail";
		$response ['result'] =  "Token expired";
		echo json_encode($response);
		die();
	}
        
}
?>