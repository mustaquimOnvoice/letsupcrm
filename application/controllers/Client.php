<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 5){
			$this->session->set_flashdata('error', 'You Are not Allowed to access...!');
			redirect('login');
		}

	}
	
	public function index(){
		$this->clients();
	}
	
	public function clients()
	{
		if($this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 4){
			$pagedata['results'] = $this->base_models->get_clients('tbl_client.*, business_cat.bname, wwc_admin.username');			
		}else{
			$pagedata['results'] = $this->base_models->get_clients('tbl_client.*, business_cat.bname, wwc_admin.username','','','','',$this->session->userdata('id'));
		}
		$pagedata['delete_link'] = 'Client/delete_client';
		$this->renderView('Admin/clients',$pagedata);
	}
	
	public function add_client()
	{
		$pagedata['business_list'] = $this->base_models->get_records('business_cat',array('id','bname'),array('status =' => '1'));		
		$this->renderView('Admin/add-client',$pagedata);
	}
	
	public function insert_client()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('bcat', 'Business Category', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('gst_no', 'GST No.', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'name'=>$this->input->post('name'),
						'user_id'=> $this->session->userdata('id'),
						'business_name'=>$this->input->post('business_name'),
						'bcat'=>$this->input->post('bcat'),
						'email'=>$this->input->post('email'),
						'mobile'=>$this->input->post('contact'),
						'gst_no'=>$this->input->post('gst_no'),
						'address'=> $this->input->post('address'),
						'added_by'=> $this->session->userdata('id'),
						'status'=> 1,
						'created_at'=> $current_date,
						// 'profile_pic'=>$data['file_name'],
						'update_on'=>$current_date
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('tbl_client',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/client/clients'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
					}
			}
			redirect(site_url('/client/add_client'));	
	}
	
	public function update_client()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('/client/clients')); 
		}
		// die($id);
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('bcat', 'Business Category', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('gst_no', 'GST No.', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->form_validation->run())
			{					
				$update_array=array(
						'name'=>$this->input->post('name'),
						'business_name'=>$this->input->post('business_name'),
						'bcat'=>$this->input->post('bcat'),
						'email'=>$this->input->post('email'),
						'mobile'=>$this->input->post('contact'),
						'gst_no'=>$this->input->post('gst_no'),
						'address'=> $this->input->post('address'),
						// 'profile_pic'=>$data['file_name'],
						'update_on'=>$current_date
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('tbl_client',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
			redirect(site_url('/client/edit_client/?id='.base64_encode($id)));
	}
	
	public function edit_client()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_clients('tbl_client.*, business_cat.bname',$id);
		$pagedata['business_list'] = $this->base_models->get_records('business_cat',array('id','bname'),array('status =' => '1'));	
		// echo '<pre>';
		// print_r($pagedata);
		// die();
		$this->renderView('Admin/edit-client',$pagedata);
	}
		
	public function delete_client()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date,
							'deleted_by'=>$this->session->userdata('id')
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('tbl_client',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	//check if already exist with same mobile no.
	public function checkMobile(){
		$id = $_POST['id'];
		//get details
		$pagedata['data']=$this->base_models->GetSingleDetails('tbl_dsr', array('id' => $id));
		$mobile = $pagedata['data']->mobile;
		
		//check if already exist with same mobile no.
		$exist = $this->base_models->GetSingleDetails('tbl_client', array('mobile' => $pagedata['data']->mobile),array('id', 'mobile','business_name'));
		$cntClient = count($exist);
		if($cntClient >=1){
			$bname = $exist->business_name;
			$data['status'] = 'error';
			$data['message'] = "Not added, $mobile number already exist with <b>$bname</b>";
			echo json_encode($data);
			die();
		}
	}
	
	//check if already exist with same mobile no.
	public function getClientsNames(){
		$id = $_POST['id'];		
		//check if already exist with same mobile no.
		$Clients = $this->base_models->GetAllValues('tbl_client', null ,array('id', 'name','business_name'));
		$data['name'] = array_column($Clients, 'name');
		$data['business_name'] = array_column($Clients, 'business_name');
		echo json_encode($data);
		die();
	}
	
}
