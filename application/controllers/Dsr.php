<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Dsr extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access...!');
			redirect($this->session->userdata('login_type').'/dashboard/');
		}

	}
	
	public function index(){
		$this->view_dsr();
	}
	
	//generate to excel	
	public function generate_adv_excel($param1,$param2){
		// create file name
		$fileName = $param1.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param2;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'UserName');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ClientName');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'BusinessName');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Mobile');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Address');       
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Call Time');       
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Type Of Call');       
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Summary');       
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['username']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['business_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['mobile']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['call_time']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['t_o_call']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['summary']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('d-M-y', strtotime($element['date'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}	
	
	public function view_dsr($param1 = '')
	{	
		//Start Where conditions
			$where = "td.status = '1' ";
			if($param1 == 'date'){						
				if(!$this->input->post('daterange')){
					redirect(site_url().'/dsr/view_dsr');
				}
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0]));
				$todate = date('Y-m-d', strtotime($ranges[1]));
				$t_o_call = (@$this->input->post('t_o_call')) ? $this->input->post('t_o_call') : '';
				$user_id = (@$this->input->post('user_id')) ? $this->input->post('user_id') : '';
		
				$where .= " AND td.dsr_date BETWEEN '$fdate' AND '$todate'";
				if($this->input->post('t_o_call') != ''){
					$where .= " AND td.t_o_call = '$t_o_call'";
				}
				if($this->input->post('user_id') != ''){
					$where .= " AND td.added_by = '$user_id'";
				}
			}
		//End Where conditions		
		
		if($this->session->userdata('user_type') == 3){//Marketing
			$where .= " AND td.added_by = ".$this->session->userdata('id');
		}
		$data['data'] = $this->base_models->get_dsr_data($where);//admin
		//Export xls
		if($param1 == 'createxls' || @$this->input->post('submit') == 'createxls'){
			$this->generate_adv_excel('Daily Sales report',$data['data']);			
		}
		
		$t_o_call = (@$t_o_call) ? $t_o_call : '';
		$user_id = (@$user_id) ? $user_id : '';
		$users = $this->base_models->get_records('wwc_admin',array('id','username'),array('type' => 3),'');
		$pagedata = array('select'=>array('t_o_call' =>$t_o_call,'user_id' =>$user_id),'results'=>$data['data'], 'users_details'=>$users, 'delete_link'=>'dsr/delete_dsr');

		$this->renderView('Dsr/view_dsr',$pagedata);
	}
	
	public function add_dsr()
	{
		$pagedata['business_list'] = $this->base_models->get_records('business_cat',array('id','bname'),array('status =' => '1'));		
		$this->renderView('Dsr/add_dsr',$pagedata);
	}
	
	public function insert_dsr()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('t_o_call', 'Type Of Call', 'trim|required');
		$this->form_validation->set_rules('call_time', 'Call Time', 'trim|required');
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile Number', 'trim|required|numeric');
		$this->form_validation->set_rules('address', 'Address', 'trim');
		$this->form_validation->set_rules('summary', 'Summary', 'trim');
		$current_date = date("Y-m-d H:i:s");

		$error='';
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'name'=>$this->input->post('name'),
						'business_name'=>$this->input->post('business_name'),
						'email'=>$this->input->post('email'),
						't_o_call'=>$this->input->post('t_o_call'),
						'call_time'=>$this->input->post('call_time'),
						'mobile'=>$this->input->post('contact'),
						'address'=> $this->input->post('address'),
						'summary'=> $this->input->post('summary'),
						'dsr_date'=> date("Y-m-d",strtotime($this->input->post('date'))),
						'added_by'=> $this->session->userdata('id'),
						'status'=> '1',
						'date'=> $current_date,
						'update_on'=>$current_date
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('tbl_dsr',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/dsr/view_dsr'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
					}
			}
			redirect(site_url('/dsr/add_dsr'));	
	}
	
	public function update_dsr()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('/dsr/view_dsr')); 
		}
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('t_o_call', 'Type Of Call', 'trim|required');
		$this->form_validation->set_rules('call_time', 'Call Time', 'trim|required');
		// $this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile Number', 'trim|required|numeric');
		$this->form_validation->set_rules('address', 'Address', 'trim');
		$this->form_validation->set_rules('summary', 'Summary', 'trim');
		$current_date = date("Y-m-d H:i:s");
		$error='';
			if($this->form_validation->run())
			{	
				if($this->input->post('date') != ''){
					$dsr_date = date("Y-m-d",strtotime($this->input->post('date')));
				}else{
					$dsr_date = date("Y-m-d",strtotime($this->input->post('dsr_date')));
				}
				
				$update_array=array(
						'name'=>$this->input->post('name'),
						'business_name'=>$this->input->post('business_name'),
						'email'=>$this->input->post('email'),
						't_o_call'=>$this->input->post('t_o_call'),
						'call_time'=>$this->input->post('call_time'),
						'mobile'=>$this->input->post('contact'),
						'address'=> $this->input->post('address'),
						'summary'=> $this->input->post('summary'),
						'dsr_date'=> $dsr_date,
						'update_on'=>$current_date
					);
				$where_array = array('id'=>$id);
				if($this->base_models->update_records('tbl_dsr',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
			redirect(site_url('/dsr/edit_dsr/?id='.base64_encode($id)));
	}
	
	public function edit_dsr()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('tbl_dsr', array('id' => $id));
		// $pagedata['business_list'] = $this->base_models->get_records('business_cat',array('id','bname'),array('status =' => '1'));	
		
		$this->renderView('Dsr/edit_dsr',$pagedata);
	}
		
	public function delete_dsr()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date,
							'deleted_by'=>$this->session->userdata('id')
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('tbl_dsr',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function viewContent(){
		$id  = $this->input->post('id');
		$result = $this->base_models->get_records('tbl_dsr',array('summary'),array('id'=>$id),true);
		$data['status'] = 'success';
		$data['message'] = $result['summary'];
		echo json_encode($data);
	}
		
	public function makeClient(){
		$id = $_POST['id'];
		//get details
		$pagedata['data']=$this->base_models->GetSingleDetails('tbl_dsr', array('id' => $id));
		$mobile = $pagedata['data']->mobile;
		
		//check if already exist with same mobile no.
		$exist = $this->base_models->GetSingleDetails('tbl_client', array('mobile' => $pagedata['data']->mobile),array('id', 'mobile','business_name'));
		$cntClient = count($exist);
		if($cntClient >=1){
			$bname = $exist->business_name;
			$data['status'] = 'error';
			$data['message'] = "Not added, $mobile number already exist with <b>$bname</b>";
			echo json_encode($data);
			die();
		}
		
		$current_date = date("Y-m-d H:i:s");
		$insert_array=array(
			'name'=> $pagedata['data']->name,
			'business_name'=> $pagedata['data']->business_name,
			'email'=> $pagedata['data']->email,
			'mobile'=> $pagedata['data']->mobile,
			'address'=> $pagedata['data']->address,
			'added_by'=> $this->session->userdata('id'),
			'status'=> '1',
			'update_on'=>$current_date
		);
		//insert new client
		if($this->base_models->add_records('tbl_client',$insert_array)){
			$data['status'] = 'success';
			$data['message'] = 'Client added Successfully. Now Please Select its category from Clients List';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Not added Please try again';
		}
		echo json_encode($data);
	}
	
}
