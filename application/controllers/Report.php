<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model('report_models');
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function index(){
		$this->citywise();
	}
	
	//generate to excel	
	public function generate_report_excel($param1,$param2){
		// create file name
		$fileName = $param1.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param2;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Edition Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Amount');      
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$amt[] = $element['rate'];
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['rate']);
			$rowCount++;
		}
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Total');
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, array_sum($amt));
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function citywise($param1 = '',$param2 = '',$param3 = '')
	{
		$this->load->helper('common_helper');
		
		//Start Where conditions
		
			$status_str = 'City Total Revenue';
			$status = '';
			$invoice_generate = '';
			$pay_status = '';
			$where = '';
			if($param1 == 'date'){						
				if(!$this->input->post('daterange')){
					redirect(site_url().'report/citywise/');
				}
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0]));
				$todate = date('Y-m-d', strtotime($ranges[1]));
				$client_id = (@$this->input->post('client_id')) ? $this->input->post('client_id') : '';
				$user_id = (@$this->input->post('user_id')) ? $this->input->post('user_id') : '';
				$invoice_generate = (@$this->input->post('invoice_generate')) ? $this->input->post('invoice_generate') : '';
				$pay_status = (@$this->input->post('pay_status') || @$this->input->post('pay_status') == 0) ? $this->input->post('pay_status') : '';
				
				$where = "ta.date BETWEEN '$fdate' AND '$todate'";
				if($this->input->post('client_id') != ''){
					$where .= "AND ta.client_id = $client_id";
				}
				if($this->input->post('user_id') != ''){
					$where .= " AND ta.user_id = $user_id";
				}
				if($this->input->post('pay_status') != ''){
					$where .= " AND ta.pay_status = '$pay_status'";
					switch($pay_status){
						case '0':
							$status_str = 'Category Total Revenue Pending';
							break;
						case '1':
							$status_str = 'Category Total Revenue Received';
							break;
					}
				}
				if($this->input->post('invoice_generate') != ''){
					$where .= " AND ta.invoice_generate = $invoice_generate";
					switch($invoice_generate){
						case '1':
							$status_str = 'City Total Revenue Invoice Approved';
							break;
						case '2':
							$status_str = 'City Total Revenue Invoice Pending';
							break;
					}
				}
				if($this->input->post('status') != ''){
					$status = $this->input->post('status');
					$where .= " AND ta.status = '$status'";
						switch($status){
							case '0':
								$status_str = 'City Total Revenue Pending';
								break;
							case '1':
								$status_str = 'City Total Revenue Reject';
								break;
							case '2':
								$status_str = 'City Total Revenue Approved';
								break;
							case '3':
								$status_str = 'City Total Revenue Publish';
								break;
							default:
								$status_str = 'City Total Revenue';
								break;
						}					
				}
			}
					
			switch($this->session->userdata('login_type')){
				case 'Marketing' :
					$where .= ' AND ta.user_id = '.$this->session->userdata('id');
					break;
			}
		//End Where conditions
		
		$data['data'] = $this->report_models->get_citywise_data($where);
		
		//Export xls
		if(@$this->input->post('submit') == 'createxls'){
			$this->generate_report_excel($status_str,$data['data']);			
		}
		
		$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),'','');
		// $citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		$users = $this->base_models->get_records('wwc_admin',array('id','username'),array('type' => 3),'');
		$client_id = (@$client_id) ? $client_id : '';
		$user_id = (@$user_id) ? $user_id : '';
		$invoice_generate = (@$invoice_generate) ? $invoice_generate : '';
		$pay_status = (@$pay_status || $pay_status==0) ? $pay_status : '';
		$pagedata = array('select'=>array('client_id' =>$client_id,'user_id' =>$user_id,'invoice_generate' =>$invoice_generate,'pay_status' =>$pay_status),'clients_details'=>$clients_details,'users_details'=>$users,'data'=>$data['data'],'status'=>$status,'title' => $status_str);

		$this->renderView('Report/citywise',$pagedata);
	}
	
	public function catwise($param1 = '',$param2 = '',$param3 = '')
	{
		$this->load->helper('common_helper');		
		//Start Where conditions		
			$status_str = 'Category Total Revenue';
			$status = '';
			$invoice_generate = '';
			$pay_status = '';
			$where = '';
			if($param1 == 'date'){						
				if(!$this->input->post('daterange')){
					redirect(site_url().'report/catwise/');
				}
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0]));
				$todate = date('Y-m-d', strtotime($ranges[1]));
				$client_id = (@$this->input->post('client_id')) ? $this->input->post('client_id') : '';
				$user_id = (@$this->input->post('user_id')) ? $this->input->post('user_id') : '';
				$invoice_generate = (@$this->input->post('invoice_generate')) ? $this->input->post('invoice_generate') : '';
				$pay_status = (@$this->input->post('pay_status') || @$this->input->post('pay_status') == 0) ? $this->input->post('pay_status') : '';
				// echo $this->input->post('pay_status');
				// die();
				
				$where = "ta.date BETWEEN '$fdate' AND '$todate'";
				if($this->input->post('client_id') != ''){
					$where .= "AND ta.client_id = $client_id";
				}
				if($this->input->post('user_id') != ''){
					$where .= " AND ta.user_id = $user_id";
				}
				if($this->input->post('pay_status') != ''){
					$where .= " AND ta.pay_status = '$pay_status'";
					switch($pay_status){
						case '0':
							$status_str = 'Category Total Revenue Pending';
							break;
						case '1':
							$status_str = 'Category Total Revenue Received';
							break;
					}
				}
				if($this->input->post('invoice_generate') != ''){
					$where .= " AND ta.invoice_generate = $invoice_generate";
					switch($invoice_generate){
						case '1':
							$status_str = 'Category Total Revenue Invoice Approved';
							break;
						case '2':
							$status_str = 'Category Total Revenue Invoice Pending';
							break;
					}
				}
				if($this->input->post('status') != ''){
					$status = $this->input->post('status');
					$where .= " AND ta.status = '$status'";
						switch($status){
							case '0':
								$status_str = 'Category Total Revenue Pending';
								break;
							case '1':
								$status_str = 'Category Total Revenue Reject';
								break;
							case '2':
								$status_str = 'Category Total Revenue Approved';
								break;
							case '3':
								$status_str = 'Category Total Revenue Publish';
								break;
							default:
								$status_str = 'Category Total Revenue';
								break;
						}					
				}
			}
					
			switch($this->session->userdata('login_type')){
				case 'Marketing' :
					$where .= ' AND ta.user_id = '.$this->session->userdata('id');
					break;
			}
		//End Where conditions		
		$data['data'] = $this->report_models->get_catwise_data($where);
		//Export xls
		if(@$this->input->post('submit') == 'createxls'){
			$this->generate_report_excel($status_str,$data['data']);			
		}
		
		$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),'','');
		// $citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		$users = $this->base_models->get_records('wwc_admin',array('id','username'),array('type' => 3),'');
		$client_id = (@$client_id) ? $client_id : '';
		$user_id = (@$user_id) ? $user_id : '';
		$invoice_generate = (@$invoice_generate) ? $invoice_generate : '';
		$pay_status = (@$pay_status || $pay_status==0) ? $pay_status : '';
		$pagedata = array('select'=>array('client_id' =>$client_id,'user_id' =>$user_id,'invoice_generate' =>$invoice_generate,'pay_status' =>$pay_status),'clients_details'=>$clients_details,'users_details'=>$users,'data'=>$data['data'],'status'=>$status,'title' => $status_str);

		$this->renderView('Report/catwise',$pagedata);
	}	
		
}
