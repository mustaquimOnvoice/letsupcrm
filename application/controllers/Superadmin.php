<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer, 7=superadmin
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 7 && $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function dashboard()
	{		
		$pagedata['total_users'] = $this->base_models->count_users();
		$pagedata['total_clients'] = $this->base_models->count_clients();
		$pagedata['total_pending_adv'] = $this->base_models->count_adv('0');
		$pagedata['total_rejected_adv'] = $this->base_models->count_adv('1');
		$pagedata['total_approved_adv'] = $this->base_models->count_adv('2');
		$pagedata['total_publish_adv'] = $this->base_models->count_adv('3');
		$pagedata['total_pending_invoice'] = $this->base_models->count_invoice('2');
		$pagedata['total_approved_invoice'] = $this->base_models->count_invoice('1');
		$pagedata['regularClient'] = $this->base_models->regularClient();
		// $pagedata['nonRegularClient'] = $this->base_models->nonRegularClient();
		$pagedata['newClientsInCurrentmonth'] = $this->base_models->newClientsInCurrentmonth();
		$pagedata['revenueCitywise'] = $this->load->view('graphs/revenue-citywise','',true);
		$pagedata['revenueCitywiseThisMonth'] = $this->load->view('graphs/revenue-citywise-thismonth','',true);
		$pagedata['revenueCitywiseLastMonth'] = $this->load->view('graphs/revenue-citywise-lastmonth','',true);
		
		// get financial date
		$todays = date('Y-m-d');
		$currentyear = date('Y');
		$financialYear = (date('m',strtotime($todays)) >= 4) ? date('Y',strtotime($todays)) + 1 : date('Y');
		$fromyear = $financialYear - 1;
		$revenueAprToMar['data'] = array('fyear' =>$fromyear, 'toyear'=>$financialYear);
		$pagedata['revenueAprToMar'] = $this->load->view('graphs/revenueAprToMar',$revenueAprToMar,true);
		// marketing userwise report
		$pagedata['revenueMarketingUserwise'] = $this->load->view('graphs/revenue-marketing-userwise','',true);
		$pagedata['revenueMarketingAprToMar'] = $this->load->view('graphs/revenueMarketingAprToMar',$revenueAprToMar,true);
		$pagedata['revenueMarketingUserLastMonth'] = $this->load->view('graphs/revenue-marketinguser-lastmonth','',true);
		$pagedata['revenueMarketingUserThisMonth'] = $this->load->view('graphs/revenue-marketinguser-thismonth','',true);
		$this->load->view('includes/head');
        $pagedata['header'] = $this->load->view('includes/header','',true);
		$pagedata['nav'] = $this->load->view('includes/superadmin-navigation','',true);
		$pagedata['footer'] = $this->load->view('includes/footer','',true);
		$this->load->view('SuperAdmin/dashboard',$pagedata);
    }
    
    public function adv($status = '0',$param1 = '',$param2 = '',$param3 = ''){
        $this->load->helper('common_helper');
		switch($status){
			case '0':
				$status_str = 'Advertsiment Pending';
				break;
			case '1':
				$status_str = 'Advertsiment Reject';
				break;
			case '2':
				$status_str = 'Advertsiment Approved';
				break;
			case '3':
				// redirect(site_url('Adv/publish/3'));
				$status_str = 'Advertsiment Publish';
				break;
		}
		
		//Start Where conditions
			$daterange = (@$this->input->post('daterange')) ? $this->input->post('daterange') : '';
			$where = '';
			if(!empty($daterange)){						
				if(!$this->input->post('daterange')){
					redirect(site_url().'adv/manage_adv/'.$status);
				}
				$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0]));
				$todate = date('Y-m-d', strtotime($ranges[1]));
				$client_id = (@$this->input->post('client_id')) ? $this->input->post('client_id') : '';
				$user_id = (@$this->input->post('user_id')) ? $this->input->post('user_id') : '';
				$pay_status = $this->input->post('pay_status');
		
				$where = "AND ta.date BETWEEN '$fdate' AND '$todate'";
				if($this->input->post('client_id') != ''){
					$where .= "AND ta.client_id = $client_id";
				}
				if($this->input->post('user_id') != ''){
					$where .= " AND ta.user_id = $user_id";
				}
				if($this->input->post('pay_status') != ''){
					$where .= " AND ta.pay_status = '$pay_status'";
				}
			}
			if($param1 == 'invoice'){		
				switch($param2){
					case '1':
						$status_str = 'Invoice Approved';
						break;
					case '2':
						$status_str = 'Invoice Pending';
						break;
				}
				$where .= ' AND ta.invoice_generate = '.$param2;
			}else{			
				switch($this->session->userdata('login_type')){
					case 'Marketing' :
						$where .= ' AND ta.user_id = '.$this->session->userdata('id');
						break;
				}
			}
			if($this->session->userdata('login_type')=='Marketing'){//For marketing
	        	$where .= ' AND ta.user_id = '.$this->session->userdata('id');	        
			}
		//End Where conditions
		$where .= " AND ta.saflag = '1' ";
		$data['data'] = $this->base_models->get_adv_data($status,$where);
		
		//Export xls
		if($param1 == 'createxls' || $param3 == 'createxls' || @$this->input->post('submit') == 'createxls'){
			$this->generate_adv_excel($status_str,$data['data']);			
		}
		
		$clients_details = $this->base_models->get_records('tbl_client',array('id','business_name'),'','');
		$citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		$users = $this->base_models->get_records('wwc_admin',array('id','username'),array('type' => 3),'');
		$client_id = (@$client_id) ? $client_id : '';
		$user_id = (@$user_id) ? $user_id : '';
		$pay_status = @$pay_status;
		$id=$this->session->userdata('id');
		$pagedata = array('select'=>array('client_id' =>$client_id,'user_id' =>$user_id,'pay_status' =>$pay_status),'clients_details'=>$clients_details,'users_details'=>$users,'citys_details'=>$citys_details,'data'=>$data['data'],'status'=>$status, 'delete_link'=>'Adv/delete_adv', 'title' => $status_str, 'invoice' => $param2);

        $this->load->view('includes/head');
        $pagedata['header'] = $this->load->view('includes/header','',true);
		$pagedata['nav'] = $this->load->view('includes/superadmin-navigation','',true);
		$pagedata['footer'] = $this->load->view('includes/footer','',true);
		$this->load->view('Adv/manage_adv_sa',$pagedata);
    }
		
}
