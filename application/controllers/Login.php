<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
public function __construct()
	{
		date_default_timezone_set('Asia/Kolkata');
		parent::__construct();
		$this->load->database();
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function check_login()
	{
		if(isset($_POST['submit']))
			{	
				$this->db->where('username',trim($this->input->post('username')));
				$this->db->where('password',md5($this->input->post('password')));
				// $this->db->where('status','1');
				
				$get_admin_details_qry = $this->db->get('wwc_admin');	
				$cnt = $get_admin_details_qry->num_rows();
				$get_admin_details_res = $get_admin_details_qry->row_array();	
				
				if($cnt >= 1){
					if($get_admin_details_res['status'] == 1){
						$newdata = array(
										'id' => $get_admin_details_res['id'],
										'user_fname' => $get_admin_details_res['fname'],
										'user_mname' => $get_admin_details_res['mname'],
										'user_lname' => $get_admin_details_res['lname'],
										'user_username' => $get_admin_details_res['username'],
										'user_email' => $get_admin_details_res['email'],
										'user_contact' => $get_admin_details_res['contact'],
										'user_profile' => $get_admin_details_res['profile_pic'],
										'user_type' => $get_admin_details_res['type'],
										'user_status' => $get_admin_details_res['status']
										);
						switch($newdata['user_type']){
							case 7 :
								$newdata['login_type'] =  'Superadmin';
								break;
							case 1 :
								$newdata['login_type'] =  'Admin';
								break;
							case 3 :
								$newdata['login_type'] =  'Marketing';
								break;
							case 4 :
								$newdata['login_type'] =  'Account';
								break;
							case 5 :
								$newdata['login_type'] =  'Operation';
								break;
							case 7 :
								$newdata['login_type'] =  'Watcher';
								break;
						}
						$this->session->set_userdata($newdata);	
						redirect($newdata['login_type'].'/dashboard/');
					}else{
						$this->session->set_flashdata('suspend', 'You Account Is Suspended...!');
						redirect('Login');
					}
				}else{
					$this->session->set_flashdata('error', 'Enter Correct Username & Password...!');
					redirect('Login');
				}						
						
			}
			
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('success', 'You Have Successfully Logout!!');
		redirect('login');
	}
}
