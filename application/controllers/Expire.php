<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expire extends CI_Controller 
{
public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function index()
	{
		$today_date=date('Y-m-d');
		$this->db->where('exp_date',$today_date);
		$pagedata=$this->db->get("tbl_users")->result_array();
		foreach($pagedata as $row )
					 {
						   $user_id=$row['user_id'];
						   $is_active=$row['is_active'];
						   
						   $data['is_active']=1;
						   
						   $this->db->where('user_id',$user_id);
						   $this->db->update('tbl_users',$data);
					 }
		
		// echo "<pre>";
		// print_r($pagedata);
		// die();
		// echo "</pre>";
		
	}
}
