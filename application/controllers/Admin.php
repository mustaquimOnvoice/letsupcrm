<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
	<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 7){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}

	// public function longPolling(){
	// 	$responce = array('name'=>'Mustaquim');
	// 	echo json_encode($responce); 
	// }

	// revenueCitywise for graph
	public function revenueCitywise()
	{
		$this->load->model('report_models');
		$responce = $this->report_models->revenueCitywise('');
		echo json_encode($responce); 
	}

	// revenueCitywiseThisMonth for graph
	public function revenueCitywiseThisMonth()
	{
		$this->load->model('report_models');
		$fromDate = date('Y-m-01');
		$toDate = date('Y-m-d');
		$where = "date BETWEEN '$fromDate' AND '$toDate'";
		$responce = $this->report_models->revenueCitywise($where);
		echo json_encode($responce);
	}

	// revenueCitywiseLastMonth for graph
	public function revenueCitywiseLastMonth()
	{
		$this->load->model('report_models');
		$lastMonth = date('m') - 1;
		$fromDate = date("Y-0$lastMonth-01");
		$toDate = date("Y-0$lastMonth-t");
		$where = "date BETWEEN '$fromDate' AND '$toDate'";
		$responce = $this->report_models->revenueCitywise($where);
		echo json_encode($responce);
	}

	// revenueAprToMar for graph Financial yearwise
	public function revenueAprToMar()
	{
		$this->load->model('report_models');
		$todays = date('Y-m-d');
		$currentyear = date('Y');
		$financialYear = (date('m',strtotime($todays)) >= 4) ? date('Y',strtotime($todays)) + 1 : date('Y');
		$fromyear = $financialYear - 1;
		$fromDate = date("$fromyear-04-01");		
		$toDate = date("$financialYear-04-01");
		$where = "date BETWEEN '$fromDate' AND '$toDate'";		
		$responce = $this->report_models->revenueCitywise($where);
		echo json_encode($responce);
	}

	// revenueMarketingUserwise for graph
	public function revenueMarketingUserwise()
	{
		$this->load->model('report_models');
		$responce = $this->report_models->revenueMarketingUserwise('');
		echo json_encode($responce); 
	}
	//revenueMarketingAprToMar
	public function revenueMarketingAprToMar()
	{
		$this->load->model('report_models');
		$todays = date('Y-m-d');
		$currentyear = date('Y');
		$financialYear = (date('m',strtotime($todays)) >= 4) ? date('Y',strtotime($todays)) + 1 : date('Y');
		$fromyear = $financialYear - 1;
		$fromDate = date("$fromyear-04-01");		
		$toDate = date("$financialYear-04-01");
		$where = "date BETWEEN '$fromDate' AND '$toDate'";		
		$responce = $this->report_models->revenueMarketingUserwise($where);
		echo json_encode($responce);
	}

	// revenueMarketingUserThisMonth for graph
	public function revenueMarketingUserThisMonth()
	{
		$this->load->model('report_models');
		$fromDate = date('Y-m-01');
		$toDate = date('Y-m-d');
		$where = "date BETWEEN '$fromDate' AND '$toDate'";
		$responce = $this->report_models->revenueMarketingUserwise($where);
		echo json_encode($responce);
	}

	// revenueMarketingUserLastMonth for graph
	public function revenueMarketingUserLastMonth()
	{
		$this->load->model('report_models');
		$lastMonth = date('m') - 1;
		$fromDate = date("Y-0$lastMonth-01");
		$toDate = date("Y-0$lastMonth-t");
		$where = "date BETWEEN '$fromDate' AND '$toDate'";
		$responce = $this->report_models->revenueMarketingUserwise($where);
		echo json_encode($responce);
	}

	public function dashboard()
	{		
		$pagedata['total_users'] = $this->base_models->count_users();
		$pagedata['total_clients'] = $this->base_models->count_clients();
		$pagedata['total_pending_adv'] = $this->base_models->count_adv('0');
		$pagedata['total_rejected_adv'] = $this->base_models->count_adv('1');
		$pagedata['total_approved_adv'] = $this->base_models->count_adv('2');
		$pagedata['total_publish_adv'] = $this->base_models->count_adv('3');
		$pagedata['total_pending_invoice'] = $this->base_models->count_invoice('2');
		$pagedata['total_approved_invoice'] = $this->base_models->count_invoice('1');
		$pagedata['regularClient'] = $this->base_models->regularClient();
		// $pagedata['nonRegularClient'] = $this->base_models->nonRegularClient();
		$pagedata['newClientsInCurrentmonth'] = $this->base_models->newClientsInCurrentmonth();
		$pagedata['revenueCitywise'] = $this->load->view('graphs/revenue-citywise','',true);
		// Citiewise report
		$pagedata['revenueCitywiseThisMonth'] = $this->load->view('graphs/revenue-citywise-thismonth','',true);
		$pagedata['revenueCitywiseLastMonth'] = $this->load->view('graphs/revenue-citywise-lastmonth','',true);		
			// get financial date
			$todays = date('Y-m-d');
			$currentyear = date('Y');
			$financialYear = (date('m',strtotime($todays)) >= 4) ? date('Y',strtotime($todays)) + 1 : date('Y');
			$fromyear = $financialYear - 1;
			$revenueAprToMar['data'] = array('fyear' =>$fromyear, 'toyear'=>$financialYear);
		$pagedata['revenueAprToMar'] = $this->load->view('graphs/revenueAprToMar',$revenueAprToMar,true);
		// marketing userwise report
		$pagedata['revenueMarketingUserwise'] = $this->load->view('graphs/revenue-marketing-userwise','',true);
		$pagedata['revenueMarketingAprToMar'] = $this->load->view('graphs/revenueMarketingAprToMar',$revenueAprToMar,true);
		$pagedata['revenueMarketingUserLastMonth'] = $this->load->view('graphs/revenue-marketinguser-lastmonth','',true);
		$pagedata['revenueMarketingUserThisMonth'] = $this->load->view('graphs/revenue-marketinguser-thismonth','',true);
		$this->renderView('Admin/dashboard',$pagedata);
	}
	
	//---------- Business cat ------------//
	public function business_cat()
	{
		$status_str = 'Business Category';
		$where_array = array('status =' => '1');
		$data['data'] = $this->base_models->get_records('business_cat',array('id','bname'),$where_array);
		$pagedata = array('data'=>$data['data'],'delete_link'=>'Admin/delete_business_cat', 'title' => $status_str);
		$this->renderView('Admin/business_cat',$pagedata);
	}
	
	public function add_business(){
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');
		if($this->form_validation->run())
		{
			$insert_array=array(
						'bname'=>$this->input->post('bname'),
						'user_id'=>$this->session->userdata('user_type'),
						'added_on'=>date("Y-m-d H:i:s")
						);
			if($this->base_models->add_records('business_cat',$insert_array)){
				$this->session->set_flashdata('success','Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}
		}
		redirect(site_url('/Admin/business_cat'));
	}
	
	public function edit_business_cat()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('business_cat', array('id' => $id), array('id','bname'));
		$this->renderView('Admin/edit-business-cat',$pagedata);
	}
	
	public function update_business_cat()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('admin/business_cat')); 
		}		
		$this->form_validation->set_rules('bname', 'Business name', 'trim|required');	
		$error='';
			if($this->form_validation->run())
			{				
				$update_array=array(
						'bname'=>$this->input->post('bname')
					);
				$where_array = array('id'=>$id);
				if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not Edited Please try again');
				}
			}
				redirect(site_url('/admin/edit_business_cat/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function delete_business_cat()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2'
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('business_cat',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End Business cat--//
	
	//---------- User ------------//
	public function users()
	{		
		$pagedata['results'] = $this->base_models->get_users();
		$pagedata['delete_link'] = 'Admin/delete_user';
		$this->renderView('Admin/users',$pagedata);
	}
	
	public function add_user()
	{
		$this->renderView('Admin/add-user');
	}
	
	public function insert_user()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_user()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_user()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('/admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_user()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- End User ------------//
	
public function add_file()
	{
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/add-file',$pagedata);
	}
	
public function upload_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file=$_FILES['file']['name'];
			$category_id=$_POST['category_id'];
			$upload_date=@date('d-m-Y');
			$status=0;
			
			$file=$file.time();
			$string = str_replace(" ","-", $file);
			// echo $string;
			// die();
			$data['file_name']=$string;
			$data['category_id']=$category_id;
			$data['upload_date']=$upload_date;
			$data['status']=$status;
			
			move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
			$this->db->insert('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Upload Successfully..!!');
			redirect('Admin/add_file/');
		}
	}

public function view_file()
	{
		$pagedata['file_details']=$this->db->query('select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id order by tu.file_id desc')->result_array();
		$this->load->view('Admin/view-file',$pagedata);
	}	

public function edit_file($file_id)
	{
		$pagedata['file_details']=$this->db->query("select * from tbl_uploads as tu JOIN tbl_category as tc on tu.category_id=tc.id where tu.file_id='$file_id'")->result_array();
		$pagedata['category_details']=$this->db->get('tbl_category')->result_array();
		$this->load->view('Admin/edit-file',$pagedata);
	}	

public function update_file()
	{
		if(isset($_POST['submit']))
		{
			
			$file_id=$_POST['file_id'];
			$old_file=$_POST['old_file'];
			$category_id=$_POST['category_id'];

			if($_FILES['file']['name']!='')
				{
					$file=$_FILES['file']['name'];
					$file=$file.time();
					$string = str_replace(" ","-", $file);
					move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$string.'.zip');
					unlink($base_url.'uploads/'.$old_file.'.zip');
				}
			else
				{
					$file=$old_file;
				}
			
			if($_POST['status']!='')
				{
					$status=$_POST['status'];
				}
			else
				{
					$status=$_POST['old_status'];
				}
				
			$data['file_name']=$file;
			$data['category_id']=$category_id;
			$data['status']=$status;
			
			$this->db->where('file_id',$file_id);
			$this->db->update('tbl_uploads',$data);
			$this->session->set_flashdata('success', 'File Update Successfully..!!');
			redirect('Admin/view_file/');
		}
	}
	
public function delete_file($old_file,$file_id)
	{
		
		$this->db->where('file_id',$file_id);
		$this->db->delete('tbl_uploads');
		unlink($base_url.'uploads/'.$old_file.'.zip');
		$this->session->set_flashdata('delete', 'File Delete Successfully..!!');
		redirect('Admin/view_file/');
	}

// public function profile()
	// {
		// $user_id=$this->session->userdata('user_id');
		// $this->db->where('user_id',$user_id);
		// $pagedata['profile_details']=$this->db->get('tbl_users')->result_array();
		// $this->load->view('Admin/profile',$pagedata);
	// }
	
// public function edit_profile()
	// {
		// if(isset($_POST['submit']))
		  // {
			// $user_id=$_POST['user_id'];
			// $name=$_POST['name'];
			// $mno=$_POST['mno'];
			// $email=$_POST['email'];
			// $address=$_POST['address'];
			
			// $data['name']=$name;
			// $data['mno']=$mno;
			// $data['email']=$email;
			// $data['address']=$address;
		
			// $this->db->where('user_id',$user_id);
			// $this->db->update('tbl_users',$data);
			// $this->session->set_flashdata('success', 'Profile Update Successfully..!!');
			// redirect('Admin/profile/');
		  // }
			
	// }

// public function change_password()
	// {
		// if(isset($_POST['submit']))
		  // {
				// //User Id Take From Sesssion
				// $users_id=$_POST['users_id'];
				// $old_password=$_POST['old_password'];
				// $password1=$_POST['old-password'];
				
				// $new_password=$_POST['new_password'];
				// $confirm_password=$_POST['confirm_password'];
				
				// $data['password']=$new_password;
				
				// $this->db->where('user_id',$users_id);
				// $this->db->update('tbl_users',$data);
				// $this->session->set_flashdata('success', 'Password Change Successfully!!!');
				// redirect('Admin/profile/');

				
		  // }
	// }

	 	
		
}
