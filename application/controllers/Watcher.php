<?php
/* READ ME FIRST
	<!-- *Different Types*-->
	User_type : 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
	Advertsiment types : 0=pending, 1=rejected, 2=approved, 3=publish
	
	Note: Load this script in every page of admin
	<!-- Sparkline chart JavaScript -->
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
	<!-- toast CSS -->
	<link href="<?php echo base_url();?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
	
	<!-- Load Common/Admin Custome JS ON each page-->
	<script src="<?php echo base_url();?>assets/js/common/common.js"></script>	
*/
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Watcher extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer, 7=Watcher
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 7){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function dashboard($param1 = '')
	{		
		$this->load->helper('common_helper');
		if($param1 == 'date'){		
			if(!$this->input->post('date')){
				redirect(site_url().'Watcher/dashboard/');
			}
			$Today_date =  date('Y-m-d', strtotime($this->input->post('date')));
		}else{
			$Today_date = date('Y-m-d');			
		}
		$citys_details = $this->base_models->get_records('tbl_city',array('id','name'),'','');
		$timmings = array('6 am','8 am Club','9 am','10 am','11 am','12 pm','1 pm','2:30 pm','3:30 pm','4:30 pm','5:30 pm','6:30 pm','7:30 pm','8:30 pm','9 pm','10 pm');
		$pagedata = array('timmings'=>$timmings,'citys_details'=>$citys_details,'Today_date'=>$Today_date, 'title' => 'Schedule');
		
		$this->renderView('Watcher/dashboard',$pagedata);
	}
}
